package main

import (
	"net/url"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

var _ OnePageUpdater = &Mangapark{}

func TestMangapark(t *testing.T) {
	assert := assert.New(t)
	var mp Mangapark

	// test pulling series out of a search page
	doc := getDoc(t, "tests/mangapark-TestSearch")
	ss := mp.GetSeriesList(doc)
	assert.NotEmpty(ss, "expected chapters but got none")
	for _, s := range ss {
		assert.NotEmpty(s.Name)
		assert.NotEmpty(s.URL)
	}

	// pulling chapters out of a volume page
	doc = getDoc(t, "tests/mangapark-TestGetChapters")
	doc.Url, _ = url.Parse("http://mangapark.me/")
	cs := mp.getChaptersFromDoc(doc, "Series")
	assert.NotEmpty(cs)

	for _, c := range cs {
		assert.NotEmpty(c.URL)
		assert.NotEmpty(c.Number)
	}

	// pulling images out of a chapter page
	doc = getDoc(t, "tests/mangapark-TestGetImageURLs")
	doc.Url, _ = url.Parse("http://mangapark.me/manga/chapter/")
	urls := mp.GetImageURLs(doc)
	assert.NotEmpty(urls)
	assert.True(len(urls) > 5)
	for _, url := range urls {
		assert.True(strings.HasPrefix(url, "http"))
		assert.True(len(url) > 10)
	}
}
