package main

import (
	"fmt"
	"strings"

	"github.com/jmoiron/goquery"
)

type Mangatraders struct {
	Site
}

var _ Updater = &Mangatraders{}
var _ OnePageUpdater = &Mangatraders{}

func (m Mangatraders) SeriesListPath() string          { return URLJoin(m.URL, "/directory/") }
func (m Mangatraders) Name() string                    { return m.Site.Name }
func (m Mangatraders) Search(term ...string) []*Series { return nil }

func (m Mangatraders) trimURL(url string) string {
	url = strings.TrimPrefix(url, "..")
	if !strings.HasPrefix(url, "/read-online/") {
		parts := strings.Split(url, "=")
		if len(parts) <= 1 {
			fmt.Printf("Unknown URL format: %s", url)
			return BaseJoin(m.URL, url)
		}
		return BaseJoin(m.URL, "/read-online/"+parts[1])
	}
	return BaseJoin(m.URL, url)
}

func (m Mangatraders) GetSeriesList(doc *goquery.Document) []*Series {
	elems := doc.Find("p.seriesList a").All()
	series := make([]*Series, 0, len(elems))

	for _, a := range elems {
		href := trim(Attr(a, "href"))
		s := &Series{
			Site: m.Name(),
			Name: trim(a.Text()),
			URL:  m.trimURL(href),
		}

		spl := strings.Split(strings.TrimRight(s.URL, "/"), "/")
		s.Key = spl[len(spl)-1]
		series = append(series, s)
	}
	vprintf("Found %d series for %s\n", len(series), m.Name())
	return series
}

func (m Mangatraders) GetChapters(series *Series) []*Chapter {
	// always print when we do this
	// log.Printf("Updating %s for series %s\n", m.Name(), series.Name)

	doc, err := GetDocument(m, series.URL)
	if err != nil {
		return []*Chapter{}
	}
	return m.getChaptersFromDoc(doc, series.Name)
}

// getChaptersFromDoc parses the provided doc for chapters.
func (m Mangatraders) getChaptersFromDoc(doc *goquery.Document, seriesName string) []*Chapter {
	base := BaseURL(doc.Url.String())
	elems := doc.Find("div.well div.row div a").All()

	chapters := make([]*Chapter, 0, len(elems))

	for _, a := range elems {
		href := trim(Attr(a, "href"))
		// their markup is shocking so check the link attr
		// it has to have read-online and chapter-x in it to be a chapter
		if !strings.Contains(href, "/read-online/") || !strings.Contains(href, "chapter-") {
			continue
		}
		href = strings.TrimSuffix(href, "/")
		parts := strings.Split(href, "/")
		href = strings.Join(parts[:len(parts)-1], "/")

		c := &Chapter{
			Site:   "mangatraders",
			Series: seriesName,
			Name:   trim(a.Text()),
			URL:    BaseJoin(base, href),
		}

		spl := strings.Split(strings.TrimRight(c.URL, "/"), "/")
		c.Number = FindNumber(spl[len(spl)-2])
		if len(c.Number) == 0 {
			c.Number = FindNumber(c.Name)
		} else {
			// remove leading junk
			c.Number = strings.TrimLeft(c.Number, " \t0")
		}
		chapters = append(chapters, c)
	}
	vprintf("Found %d chapters on mangatraders\n", len(chapters))
	return chapters
}

func (m Mangatraders) GetPageURLs(doc *goquery.Document) []string {
	// XXX: this is only valid if we are not using onepage, but we almost
	// certainly are.
	options := doc.Find("#changePageSelect option").All()
	if len(options) == 0 {
		return []string{}
	}
	options = options[1:]
	urls := make([]string, 0, len(options))
	base := BaseURL(doc.Url.String())

	for _, o := range options {
		fragment := Attr(o, "value")
		urls = append(urls, BaseJoin(base, fragment))
	}

	return urls
}

func (m Mangatraders) GetImageURL(doc *goquery.Document) string {
	return Attr(doc.Find("div > a > img"), "src")
}

// optional API to get all image URLs off one page.
func (m Mangatraders) GetImageURLs(doc *goquery.Document) []string {
	var srcs []string
	for _, o := range doc.Find("div > p > img").All() {
		srcs = append(srcs, Attr(o, "src"))
	}
	return srcs
}
