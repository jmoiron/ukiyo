package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"syscall"

	"github.com/jmoiron/go-pkg-optarg"
)

const VERSION = "0.1b"

var opts struct {
	Help                 bool
	Version              bool
	Verbose              bool
	DbShell              bool
	Update               bool
	Force                bool
	SetDownloadPath      string
	Download             bool
	Overwrite            bool
	Site                 string
	ListSites            bool
	AddSite              string
	RemoveSite           string
	SetSitePriority      string
	ToggleUseUnderscores bool
	UseActiveSearches    bool
	List                 bool
	Add                  bool
	Remove               bool
	Set                  bool
	Sync                 bool
	Search               bool
	Show                 bool
	AddSeries            bool
	Log                  bool
	Clear                bool
	Filter               *Filter
}

// print only if verbose is on
func vprintf(s string, x ...interface{}) {
	if opts.Verbose {
		log.Printf(s, x...)
	}
}

func main() {
	parseopts()

	if opts.Help {
		optarg.Usage()
		return
	}
	if opts.Version {
		fmt.Printf("%s\n", VERSION)
		return
	}

	config.init()
	defer config.db.Close()
	initUpdater()

	if opts.DbShell {
		sqlite3, err := exec.LookPath("sqlite3")
		if err != nil {
			log.Fatal("Could not find `sqlite3` executable (required for --dbshell) in your path.")
		}
		fmt.Printf("sqlite3 %s\n", config.path)
		err = syscall.Exec(sqlite3, []string{sqlite3, config.path}, os.Environ())
		if err != nil {
			log.Fatal(err)
		}
		return
	}

	if len(opts.SetDownloadPath) > 0 {
		config.set("DownloadPath", opts.SetDownloadPath)
		return
	}

	if opts.ToggleUseUnderscores {
		if config.UseUnderscores {
			config.set("UseUnderscores", "0")
		} else {
			config.set("UseUnderscores", "1")
		}
		return
	}

	if len(opts.SetSitePriority) > 0 {
		if len(optarg.Remainder) != 1 {
			fmt.Printf("Error: --set-site-priority requires a name and a priority.\n")
			return
		}
		priority, err := strconv.ParseInt(optarg.Remainder[0], 10, 32)
		if err != nil {
			fmt.Printf("Error: priority must be a valid integer.\n")
			return
		}
		SetSitePriority(opts.SetSitePriority, int(priority))
		return
	}

	if opts.Clear {
		ClearChapters()
		return
	}

	if opts.ListSites {
		ListSites()
		return
	}

	if len(opts.AddSite) > 0 {
		var priority int
		var err error
		name := opts.AddSite
		if len(optarg.Remainder) == 0 {
			fmt.Printf("Error: --add-site requires at least a name and a url for argument.")
			return
		}
		url := optarg.Remainder[0]

		// set the optional priority, or use the max
		if len(optarg.Remainder) > 1 {
			priority, err = strconv.Atoi(optarg.Remainder[1])
			if err != nil {
				fmt.Printf("Error with priority argument: %s\n", err)
			}
		} else {
			row := config.db.QueryRow("select max(priority) from sites")
			err = row.Scan(&priority)
			if err != nil {
				priority = 1
			} else {
				priority += 1
			}
		}

		AddSite(name, url, priority)
		return
	}

	if opts.Log {
		if len(optarg.Remainder) > 1 {
			fmt.Printf("--log takes one optional argument (number of lines)\n")
			return
		}
		ShowLog(optarg.Remainder...)
		return
	}

	if opts.List {
		ShowWatchlist()
		return
	}

	if opts.Add {
		AddWatchlist(optarg.Remainder...)
		return
	}

	if opts.Remove {
		RemoveWatchlist(optarg.Remainder...)
		return
	}

	if opts.Set {
		if len(optarg.Remainder) < 2 {
			fmt.Printf("--set requires a name and a number.\n")
			return
		}
		number := optarg.Remainder[len(optarg.Remainder)-1]
		name := strings.Join(optarg.Remainder[:len(optarg.Remainder)-1], " ")
		SetWatchlist(name, number)
		return
	}

	if len(opts.RemoveSite) > 0 {
		RemoveSite(opts.RemoveSite)
		return
	}

	if opts.Update {
		Update()
		if !opts.Download {
			return
		}
	}

	if opts.Download {
		DownloadNewChapters()
		return
	}

	if opts.Search {
		Search(optarg.Remainder...)
		return
	}

	if opts.Show {
		Show(optarg.Remainder...)
		return
	}

	if opts.AddSeries {
		AddSeries(optarg.Remainder...)
		return
	}

	// If there are no arguments, show usage
	if len(optarg.Remainder) == 0 {
		optarg.Usage()
		return
	}

	DownloadChapters(optarg.Remainder...)
}

func Update() {
	UpdateSites(opts.Force)
	w := new(Watchlist)
	w.update(opts.Force)
}

func ShowLog(num ...string) {
	nlines := 20
	if len(num) == 1 {
		nlines, _ = strconv.Atoi(num[0])
	}
	messages := config.GetLog(nlines)
	for _, m := range messages {
		fmt.Printf("%s: %s\n", toDate(m.Timestamp), m.Message)
	}
}

func DownloadNewChapters() {
	w := new(Watchlist)
	updates := w.FindNew()

	for _, update := range updates {
		for _, chapter := range update.Available {
			DownloadChapter(chapter)
			err := w.Set(chapter.Series, chapter.Number)
			if err != nil {
				fmt.Printf("Error setting %s to %s: %v\n", chapter.Series, chapter.Number, err)
			}
		}
	}
}

func SetSitePriority(name string, priority int) {
	config.SetSitePriority(name, priority)
}

func ListSites() {
	sites := []*Site{}
	config.db.Select(&sites, "SELECT name, url, priority FROM sites ORDER BY priority")
	for _, site := range sites {
		fmt.Printf(" %d. %s [%s]\n", site.Priority, site.Name, site.URL)
	}
}

func Search(terms ...string) {
	UpdateSites(false)

	series, err := FindSeries(opts.Site, terms...)

	if err != nil {
		log.Printf("Search: %s\n", err)
		return
	}

	for _, s := range series {
		fmt.Printf(" * %s (%s)\n", s.Name, s.Site)
	}
}

func ShowWatchlist() {
	w := new(Watchlist)
	updates := w.FindNew()
	for _, update := range updates {
		name := White(update.Series.Name, true)
		amt := fmt.Sprintf("%d", len(update.Available))
		numbers := make([]string, len(update.Available))
		for i, c := range update.Available {
			numbers[i] = c.Number
		}
		if len(update.Available) == 0 {
			fmt.Printf(" * %s [c%s, +%s]\n", name, update.Series.Number, Red(amt, true))
		} else if len(update.Available) < 5 {
			fmt.Printf(" * %s [c%s, +%s] %s\n", name, update.Series.Number, Yellow(amt, true), strings.Join(numbers, ", "))
		} else {
			fmt.Printf(" * %s [c%s, +%s] %s\n", name, update.Series.Number, Green(amt, true), strings.Join(numbers, ", "))
		}
	}
}

func AddWatchlist(terms ...string) {
	term := strings.Join(terms, " ")
	w := new(Watchlist)

	series, err := FindOneSeries(opts.Site, term)
	if err != nil {
		return
	}
	if len(series) == 0 {
		fmt.Printf("No series found matching term %s\n", Yellow(term, true))
		return
	}
	if len(series) > 1 {
		e := fmt.Sprintf("Found %d series matching term %s:\n", len(series), Yellow(term, true))
		for _, s := range series {
			e += fmt.Sprintf(" * %s (%s)\n", s.Name, s.Site)
		}
		fmt.Printf(e)
		return
	}
	// if it's a duplicate, return an error
	match := series[0]
	for _, s := range w.Items {
		if s.Name == match.Name {
			fmt.Printf("Series \"%s\" already being watched.", Yellow(s.Name, true))
			return
		}
	}

	fmt.Printf("Adding series %s\n", Yellow(match.Name, true))
	w.Add(match.Name)
}

func RemoveWatchlist(terms ...string) {
	term := strings.Join(terms, " ")
	w := new(Watchlist)

	series, err := FindOneSeries(opts.Site, term)
	if err != nil {
		return
	}
	if len(series) == 0 {
		fmt.Printf("No series found matching term %s\n", Yellow(term, true))
		return
	}
	if len(series) > 1 {
		e := fmt.Sprintf("Found %d series matching term %s:\n", len(series), Yellow(term, true))
		for _, s := range series {
			e += fmt.Sprintf(" * %s (%s)\n", s.Name, s.Site)
		}
		fmt.Printf(e)
		return
	}

	match := series[0]
	fmt.Printf("Removing series %s\n", Yellow(match.Name, true))
	w.Remove(match.Name)
}

func SetWatchlist(name string, number string) {
	w := new(Watchlist)
	err := w.Set(name, number)
	if err != nil {
		fmt.Printf("%v\n", err)
	} else {
		fmt.Printf("Set series matching %s to %s\n", Yellow(name, true), number)
	}
}

func ClearChapters() {
	tx := config.db.MustBegin()
	Execf(tx, "drop table chapters;")
	Execf(tx, "CREATE TABLE IF NOT EXISTS "+createTables["chapters"])
	//Execf(tx, "drop table series;")
	//Execf(tx, createTables["series"])
	tx.Commit()
}

func Show(terms ...string) {
	UpdateSites(opts.Force)

	series, err := FindOneSeries(opts.Site, terms...)

	if err != nil {
		fmt.Printf("Error searching for terms: %s\n", err)
	}

	if len(series) == 0 {
		fmt.Printf("No series found matching term %s\n", strings.Join(terms, " "))
		return
	}
	if len(series) > 1 {
		fmt.Printf("Found %d series matching term \"%s\":\n", len(series), strings.Join(terms, " "))
		for _, s := range series {
			fmt.Printf(" * %s (%s)\n", s.Name, s.Site)
		}
		fmt.Printf("Try using the exact name (or key), or using the index (from zero) in the list.\n")
		return
	}

	chapters := GetChapters(series[0])
	for _, c := range chapters {
		if opts.Filter.Match(c.Number) {
			fmt.Printf(" * %s %s (%s)\n", c.Series, c.Number, c.Site)
		}
	}
}

func AddSeries(terms ...string) {
	series, err := FindOneSeries(opts.Site, terms...)

	if err != nil {
		fmt.Printf("Error searching for terms: %s\n", err)
		return
	}

	if len(series) == 0 {
		fmt.Printf("No series found matching term %s\n", strings.Join(terms, " "))
		return
	}

	if len(series) > 1 {
		fmt.Printf("Found %d series matching term \"%s\":\n", len(series), strings.Join(terms, " "))
		for _, s := range series {
			fmt.Printf(" * %s (%s)\n", s.Name, s.Site)
		}
		fmt.Printf("Try using the exact name (or key), or using the index (from zero) in the list.\n")
		return
	}

	upsertSeries(series...)
}

func DownloadChapters(terms ...string) {
	UpdateSites()
	series, err := FindOneSeries(opts.Site, terms...)
	if err != nil {
		fmt.Printf("Error searching for terms: %s\n", err)
	}

	if len(series) == 0 {
		fmt.Printf("No series found matching term %s\n", strings.Join(terms, " "))
		return
	}
	if len(series) > 1 {
		fmt.Printf("Found %d series matching term \"%s\":\n", len(series), strings.Join(terms, " "))
		for _, s := range series {
			fmt.Printf(" * %s (%s)\n", s.Name, s.Site)
		}
		fmt.Printf("Try using the exact name (or key) of a series.\n")
		return
	}

	chapters := GetChapters(series[0])
	for _, c := range chapters {
		if opts.Filter.Match(c.Number) {
			DownloadChapter(c)
		}
	}

}

func AddSite(name, url string, priority int) {
	err := config.AddSite(name, url, priority)
	if err != nil {
		fmt.Printf("Error adding site: %s\n", err)
	}
}

func RemoveSite(name string) {
	err := config.RemoveSite(name)
	if err != nil {
		fmt.Printf("Error removing site: %s\n", err)
	}
}

func parseopts() {
	optarg.HeaderFmt = "\n  %s options"

	optarg.Add("h", "help", "Show help.", false)
	optarg.Add("", "version", "Show version and exit.", false)
	optarg.Add("v", "verbose", "Show more output.", false)
	optarg.Add("", "dbshell", "Open a db shell on ukiyo's config db (experienced users only).", false)
	optarg.Add("", "clear", "Clear cached site data.", false)

	optarg.Header("Downloading")
	optarg.Add("u", "update", "Update all site & series info.", false)
	optarg.Add("", "sync", "Sync series info with what is on disk.", false)
	optarg.Add("d", "download", "Download new chapters from series.", false)
	optarg.Add("", "force", "Use with --update to force updating.", false)
	optarg.Add("o", "overwrite", "Overwrite previous downloads.", false)
	optarg.Add("", "set-download-path",
		fmt.Sprintf("Change destination for sync and downloads. (Current: %s)", config.DownloadPath), "")
	optarg.Add("", "toggle-use-underscores",
		fmt.Sprintf("Use undescores instead of spaces for downloads. (Current: %v)", config.UseUnderscores), false)
	optarg.Add("", "log", "Show the download log", false)

	optarg.Header("Sites")
	optarg.Add("", "sites", "List sites.", false)
	optarg.Add("", "add-site", "<name> <url> [priority], Add a site.", "")
	optarg.Add("", "rm-site", "<name>, Remove a site.", "")
	optarg.Add("", "set-site-priority", "<name> <priority>, Set download priority.", "")
	optarg.Add("", "site", "<name>, restrict to one site.", "")
	optarg.Add("c", "active-search", "use sites which cannot be searched locally.", false)

	optarg.Header("Series")
	optarg.Add("l", "list", "List series being followed.", false)
	optarg.Add("a", "add", "Add a series to the follow list.", false)
	optarg.Add("r", "remove", "Remove a series from the follow list.", false)
	optarg.Add("", "del", "Alias for --remove", false)
	optarg.Add("", "set", "Set a chapter for a series.", false)
	optarg.Add("", "find", "Find a series.", false)
	optarg.Add("", "search", "Alias for --find.", false)

	optarg.Header("Chapters")
	optarg.Add("s", "show", "Show chapters from a series.", false)
	optarg.Add("f", "filter", "Filter chapters to show or download.", "")
	optarg.Add("", "add-series", "Add series, making it available for watchlists.", false)

	opts.Filter = CompileFilter("")

	for opt := range optarg.Parse() {
		switch opt.Name {
		case "help":
			opts.Help = opt.Bool()
		case "version":
			opts.Version = opt.Bool()
		case "verbose":
			opts.Verbose = opt.Bool()
		case "dbshell":
			opts.DbShell = opt.Bool()
		// downloading
		case "update":
			opts.Update = opt.Bool()
		case "force":
			opts.Force = opt.Bool()
		case "sync":
			opts.Sync = opt.Bool()
		case "download":
			opts.Download = opt.Bool()
		case "set-download-path":
			opts.SetDownloadPath = opt.String()
		case "toggle-use-underscores":
			opts.ToggleUseUnderscores = opt.Bool()
		case "log":
			opts.Log = opt.Bool()
		case "clear":
			opts.Clear = opt.Bool()
		case "overwrite":
			opts.Overwrite = opt.Bool()
		// sites
		case "sites":
			opts.ListSites = opt.Bool()
		case "add-site":
			opts.AddSite = opt.String()
		case "rm-site":
			opts.RemoveSite = opt.String()
		case "set-site-priority":
			opts.SetSitePriority = opt.String()
		case "site":
			opts.Site = opt.String()
		case "active-search":
			opts.UseActiveSearches = opt.Bool()
		// series
		case "list":
			opts.List = opt.Bool()
		case "add":
			opts.Add = opt.Bool()
		case "remove", "del":
			opts.Remove = opt.Bool()
		case "set":
			opts.Set = opt.Bool()
		case "find", "search":
			opts.Search = opt.Bool()
		// chapters
		case "show":
			opts.Show = opt.Bool()
		case "filter":
			opts.Filter = CompileFilter(opt.String())
		case "add-series":
			opts.AddSeries = opt.Bool()
		}
	}

}
