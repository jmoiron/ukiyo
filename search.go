// This file contains functions and utilities which aid in searching and
// filtering cached Chapter and Series data.

package main

import (
	"fmt"
	"log"
	"regexp"
	"strconv"
	"strings"
)

// Return a list of all series matching terms.  If group is true, series are
// grouped by their names and repeating fields are concatted w/ csv
func FindSeriesSQL(site string, terms ...string) ([]*Series, error) {
	series := make([]*Series, 0, 10)
	term := fmt.Sprintf("%%%s%%", strings.Join(terms, "%"))
	var err error

	q := `SELECT name,
		group_concat(key) as key,
		group_concat(url) as url,
		group_concat(site) as site,
		updated
	FROM series WHERE `

	if len(site) == 0 {
		q += `name LIKE ? OR key LIKE ? GROUP BY NAME`
		err = config.db.Select(&series, q, term, term)
	} else {
		q += `site = ? AND (name LIKE ? OR key LIKE ?) GROUP BY NAME`
		err = config.db.Select(&series, q, site, term, term)
	}
	return series, err
}

// FindSeriesHttp uses the provided site updater to search for the terms.
// For now, this is only valid when the site is provided, as the result lists
// will clash with the sql results in a way that isn't really fixable.
func FindSeriesHttp(site string, terms ...string) ([]*Series, error) {
	if len(site) == 0 {
		log.Fatalf("-c requires --site as well.")
	}
	u, ok := UpdaterRegistry[site]
	if !ok {
		return []*Series{}, fmt.Errorf("Could not find updater for site %s in the registry.", site)
	}
	series := u.Search(terms...)
	return series, nil
}

func FindSeries(site string, terms ...string) ([]*Series, error) {
	if opts.UseActiveSearches {
		vprintf("Using active search as requested.")
		return FindSeriesHttp(site, terms...)
	}
	return FindSeriesSQL(site, terms...)
}

// Like the series above, but attempts to find an exact match for the
// terms being searched for;  if the name or the key is a case insensitive
// full string match to the terms, the returned array will contain only that
// If the final term is an integer, it's assumed to be the index in the
// series search to select.
func FindOneSeries(site string, terms ...string) ([]*Series, error) {
	// see if the final term is an index;  if it is, save it but remove it from
	// the terms that we actually use to search.
	index, err := strconv.Atoi(terms[len(terms)-1])
	if err != nil {
		index = -1
	} else {
		terms = terms[:len(terms)-1]
	}

	series, err := FindSeries(site, terms...)
	if len(series) < 2 {
		return series, err
	}

	if index >= 0 && len(series) > index {
		vprintf("Index of %d detected, returning series \"%v\"", index, series[index].Name)
		return []*Series{series[index]}, nil
	}

	exact := strings.ToLower(strings.Join(terms, " "))
	var lname, lkey string

	for _, s := range series {
		lname = strings.ToLower(s.Name)
		lkey = strings.ToLower(strings.Split(s.Key, ",")[0])
		if exact == lname || exact == lkey {
			return []*Series{s}, nil
		}
	}

	return series, err
}

// Like FindSeries, this attempts to find a chapters with a given series name
func FindChapters(group bool, series string) ([]*Chapter, error) {
	chapters := make([]*Chapter, 0)
	q := `select name, number, group_concat(url), series, group_concat(site)
		from chapters where series=?`
	if group {
		q += " group by number"
	}
	q += " order by round(number, 2)"

	rows, err := config.db.Query(q, series)
	if err != nil {
		return chapters, err
	}

	for rows.Next() {
		c := new(Chapter)
		err = rows.Scan(&c.Name, &c.Number, &c.URL, &c.Series, &c.Site)
		if err != nil {
			fmt.Printf("Error with row: %s\n", err)
		}
		c.Numberf, _ = strconv.ParseFloat(c.Number, 64)
		chapters = append(chapters, c)
	}

	return chapters, nil
}

// A rule, which filters are made up of
type Rule struct {
	singular bool
	not      bool
	lt       bool
	gt       bool
	lte      bool
	gte      bool
	rng      bool
	lhs      string
	rhs      string
	lhsf     float64
	rhsf     float64
}

var ruleRegex = regexp.MustCompile(`(!|<|<=|>|>=)?([\.\d]+)(?:-([\.\d]+))?`)

// A filter, which has the original string and the string compiled into
// A list of rules
type Filter struct {
	orig     string
	compiled []*Rule
}

func CompileFilter(str string) *Filter {
	f := new(Filter)
	f.orig = str
	if len(str) > 0 {
		rulestrs := strings.Split(str, ",")
		f.compiled = make([]*Rule, len(rulestrs))
		for i, r := range rulestrs {
			f.compiled[i] = CompileRule(r)
		}
	}
	return f
}

func (f *Filter) Match(arg string) bool {
	farg, err := strconv.ParseFloat(arg, 64)
	if err != nil {
		return f.ApplyString(arg)
	}
	return f.ApplyFloat(farg)
}

func (f *Filter) ApplyString(arg string) bool {
	if len(f.compiled) == 0 {
		return true
	}
	for _, r := range f.compiled {
		if r.ApplyString(arg) {
			return true
		}
	}
	return false
}

func (f *Filter) ApplyFloat(arg float64) bool {
	if len(f.compiled) == 0 {
		return true
	}
	for _, r := range f.compiled {
		if r.ApplyFloat(arg) {
			return true
		}
	}
	return false
}

func (r *Rule) ApplyString(arg string, negate ...bool) bool {
	if r.not && len(negate) == 0 {
		return !r.ApplyString(arg, true)
	}

	if r.singular && !(r.lt || r.gt || r.lte || r.gte) {
		return arg == r.lhs
	}
	if r.rng {
		return r.lhs <= arg && r.rhs >= arg
	}
	if r.lt {
		return arg < r.lhs
	}
	if r.gt {
		return arg > r.lhs
	}
	if r.lte {
		return arg <= r.lhs
	}
	if r.gte {
		return arg >= r.lhs
	}
	return false
}

func (r *Rule) ApplyFloat(arg float64, negate ...bool) bool {
	// if it's a singular rule, return if it is equal (or not)
	if r.not && len(negate) == 0 {
		return !r.ApplyFloat(arg, true)
	}
	if r.singular && !(r.lt || r.gt || r.lte || r.gte) {
		return arg == r.lhsf
	}
	if r.rng {
		return r.lhsf <= arg && r.rhsf >= arg
	}
	if r.lt {
		return arg < r.lhsf
	}
	if r.gt {
		return arg > r.lhsf
	}
	if r.lte {
		return arg <= r.lhsf
	}
	if r.gte {
		return arg >= r.lhsf
	}
	return false
}

func CompileRule(str string) *Rule {
	rule := new(Rule)
	str = strings.Trim(str, " \t")
	groups := ruleRegex.FindStringSubmatch(str)
	if groups == nil {
		fmt.Printf("Error matching rule %s!\n", str)
		return rule
	}

	matches := groups[1:]

	if len(matches[0]) != 0 && len(matches[2]) != 0 {
		fmt.Printf("Error: rule cannot combine range and not/gt/lt\n")
		return rule
	}

	rule.singular = true
	switch matches[0] {
	case "!":
		rule.not = true
	case "<":
		rule.lt = true
	case ">":
		rule.gt = true
	case "<=":
		rule.lte = true
	case ">=":
		rule.gte = true
	}

	rule.lhs = matches[1]
	rule.rhs = matches[2]
	rule.lhsf, _ = strconv.ParseFloat(rule.lhs, 64)

	if len(rule.rhs) != 0 {
		rule.singular = false
		rule.rng = true
		rule.rhsf, _ = strconv.ParseFloat(rule.rhs, 64)
	}

	return rule
}
