#!/bin/bash

mkdir -p ./tests/

function dl() {
    dest="$1"
    url="$2"
    if [ -f "$dest" ]; then
        echo "Already have $dest, skipping"
        return
    fi
    curl -s --compressed -o "$dest" "$url"
}

#batoto="http://bato.to"
#tb="tests/batoto"
#dl "$tb-TestGetImageUrl" "$batoto/read/_/142287/watashi-no-mahoutsukai-sama_by_intercross/9"
#dl "$tb-TestGetPageUrls" "$batoto/read/_/220123/blood-yakou-joushi_ch3_by_nodeadline"
#dl "$tb-TestGetChapters" "$batoto/comic/_/comics/one-piece-r39"
#dl "$tb-TestSearch" "$batoto/search_ajax?name=tsubasa"

mangahere="http://www.mangahere.co"
tm="tests/mangahere"
dl "$tm-TestDetectLicensed1" "$mangahere/manga/one_piece/"
dl "$tm-TestDetectLicensed2" "$mangahere/manga/naruto/"
dl "$tm-TestDetectNotLicensed1" "$mangahere/manga/chihayafuru/"
dl "$tm-TestDetectNotLicensed2" "$mangahere/manga/taiyou_no_ie/"
dl "$tm-TestGetImageUrl" "$mangahere/manga/taiyou_no_ie/v05/c020/"
dl "$tm-TestGetChapters" "$mangahere/manga/akame_ga_kiru/"
dl "$tm-TestGetPageUrls" "$mangahere/manga/akame_ga_kiru/c024/"
dl "$tm-SeriesList" "$mangahere/mangalist/"

mangatraders="http://mangatraders.biz"
mt="tests/mangatraders"
dl "$mt-TestGetImageUrl" "$mangatraders/read-online/Yaoguai-Mingdan-chapter-287-page-1.html"
dl "$mt-TestGetChapters" "$mangatraders/manga/Yaoguai-Mingdan"
dl "$mt-SeriesList" "$mangatraders/directory/"

mangapark="http://mangapark.me"
tp="tests/mangapark"
dl "$tp-TestGetImageURLs" "$mangapark/manga/monku-no-tsukeyou-ga-nai-rabukome-suzuki-daisuke/s2/c1/"
dl "$tp-TestGetChapters" "$mangapark/manga/monku-no-tsukeyou-ga-nai-rabukome-suzuki-daisuke"
dl "$tp-TestSearch" "$mangapark/search?orderby=views&q=one+piece"

readcomiconline="http://readcomiconline.to"
dl "tests/cloudflare.html" $readcomiconline

comicextra="http://www.comicextra.com"
dl "tests/comicextra-Search.html" "$comicextra/comic-search?key=thor"
dl "tests/comicextra-Chapters.html" "$comicextra/comic/the-mighty-thor-2016"
dl "tests/comicextra-Images.html" "$comicextra/the-mighty-thor-2016/chapter-2/full"


