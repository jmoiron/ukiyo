package main

import (
	"strings"

	"github.com/jmoiron/goquery"
)

type Mangahere struct {
	Site
}

var _ Updater = &Mangahere{}

func (m Mangahere) SeriesListPath() string          { return URLJoin(m.URL, "/mangalist/") }
func (m Mangahere) Name() string                    { return m.Site.Name }
func (m Mangahere) Search(term ...string) []*Series { return nil }

func (m Mangahere) GetSeriesList(doc *goquery.Document) []*Series {
	elems := doc.Find("div.list_manga li a").All()
	series := make([]*Series, 0, len(elems))

	for _, a := range elems {
		s := &Series{
			Site: m.Name(),
			Name: trim(a.Text()),
			URL:  BaseJoin(m.URL, trim(Attr(a, "href"))),
		}

		spl := strings.Split(strings.TrimRight(s.URL, "/"), "/")
		s.Key = spl[len(spl)-1]
		series = append(series, s)
	}
	vprintf("Found %d series for %s\n", len(series), m.Name())
	return series
}

func (m Mangahere) GetChapters(series *Series) []*Chapter {
	// always print when we do this
	// log.Printf("Updating %s for series %s\n", m.Name(), series.Name)

	doc, err := GetDocument(m, series.URL)
	if err != nil {
		return []*Chapter{}
	}
	return m.getChaptersFromDoc(doc, series.Name)
}

func (m Mangahere) isLicensed(doc *goquery.Document) bool {
	divs := doc.Find(".detail_list div").All()
	for _, div := range divs {
		s := div.Text()
		if strings.Contains(s, "licensed") && strings.Contains(s, "not available") {
			return true
		}
	}
	return false
}

// getChaptersFromDoc parses the provided doc for chapters.
func (m Mangahere) getChaptersFromDoc(doc *goquery.Document, seriesName string) []*Chapter {
	if m.isLicensed(doc) {
		return []*Chapter{}
	}
	base := BaseURL(doc.Url.String())
	elems := doc.Find(".detail_list li a").All()

	chapters := make([]*Chapter, 0, len(elems))

	for _, a := range elems {
		c := &Chapter{
			Site:   "mangahere",
			Series: seriesName,
			Name:   trim(a.Text()),
			URL:    BaseJoin(base, trim(Attr(a, "href"))),
		}

		spl := strings.Split(strings.TrimRight(c.URL, "/"), "/")
		c.Number = FindNumber(spl[len(spl)-1])
		if len(c.Number) == 0 {
			c.Number = FindNumber(c.Name)
		} else {
			// remove leading junk
			c.Number = strings.TrimLeft(c.Number, " \t0")
		}
		chapters = append(chapters, c)
	}
	vprintf("Found %d chapters on mangahere\n", len(chapters))
	return chapters
}

func (m Mangahere) GetPageURLs(doc *goquery.Document) []string {
	options := doc.Find(".readpage_top .go_page option").All()
	if len(options) == 0 {
		return []string{}
	}
	options = options[1:]
	urls := make([]string, 0, len(options))
	base := BaseURL(doc.Url.String())

	for _, o := range options {
		fragment := Attr(o, "value")
		urls = append(urls, BaseJoin(base, fragment))
	}

	return urls
}

func (m Mangahere) GetImageURL(doc *goquery.Document) string {
	return Attr(doc.Find("#image"), "src")
}
