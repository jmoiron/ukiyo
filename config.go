// abstracts access to the config database, which is also used to store series
// and chapter information so that not all queries are live against all sites

package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
)

const (
	searchTypeDirectory = iota
	searchTypeHTTP
)

type Xdg struct {
	DataHome    string
	ConfigHome  string
	Initialized bool
}

type Site struct {
	Name       string
	URL        string
	Priority   int
	SearchType int `db:"searchType"`
	Updated    int64
}

type Config struct {
	DownloadPath   string
	SiteOrder      []string
	UseUnderscores bool
	path           string
	db             *sqlx.DB
}

type Series struct {
	Name    string
	URL     string
	Site    string
	Key     string
	Updated int64
}

type Chapter struct {
	Series  string
	Site    string
	Name    string
	URL     string
	Number  string
	Numberf float64
}

func (c Chapter) String() string {
	return fmt.Sprintf("%#v", c)
}

func (c Chapter) SeriesString() string {
	if config.UseUnderscores {
		return strings.Replace(c.Series, " ", "_", -1)
	}
	return c.Series
}

type Log struct {
	Timestamp int64
	Message   string
}

var xdg Xdg
var config Config
var DefaultDownloadPath = expandpath("~/Downloads")

func DefaultFalse(arg ...bool) bool {
	ret := false
	if len(arg) != 0 {
		ret = arg[0]
	}
	return ret
}

// getenv returns the environ value for key, or def if not found.
func getenv(key, def string) string {
	value := os.Getenv(key)
	if len(value) > 0 {
		return value
	}
	return def
}

// initialize platform dependent data/config paths
func (x *Xdg) init() {
	if x.Initialized {
		return
	}
	switch runtime.GOOS {
	case "linux":
		x.DataHome = getenv("XDG_DATA_HOME", expandpath("~/.local/share"))
		x.ConfigHome = getenv("XDG_CONFIG_HOME", expandpath("~/.config"))
	case "darwin", "osx":
		x.DataHome = expandpath("~/Library/Application Support/")
		x.ConfigHome = expandpath("~/Library/Preferences/")
	}

	x.Initialized = true
}

func Execf(e sqlx.Execer, sql string, args ...interface{}) sql.Result {
	r, err := e.Exec(sql, args...)
	if err != nil {
		log.Fatal(err)
	}
	return r
}

func (c *Config) get(key string) (string, error) {
	var value string
	err := c.db.Get(&value, "SELECT value FROM config WHERE key=?", key)
	return value, err
}

func (c *Config) set(key, val string) {
	Execf(c.db, "update config set value=? where key=?", val, key)
}

func (c *Config) init() {
	xdg.init()

	configPath := filepath.Join(xdg.ConfigHome, "ukiyo/")
	os.MkdirAll(configPath, 0755)
	c.path = filepath.Join(configPath, "config.db")
	vprintf("Using config: %s\n", c.path)

	c.db = sqlx.MustConnect("sqlite3", c.path)
	c.initDb()

	var err error
	c.DownloadPath, err = c.get("DownloadPath")
	if err != nil {
		fmt.Errorf("Could not read key 'DownloadPath' from config: %q\n", err)
	}
	useUnderscores, err := c.get("UseUnderscores")
	if err != nil {
		fmt.Errorf("Could not read key 'UseUnderscores' from config: %q\n", err)
	}
	c.UseUnderscores = useUnderscores == "1"

	err = c.db.Select(&c.SiteOrder, "SELECT name FROM sites ORDER BY priority")
	if err != nil {
		fmt.Printf("Error: %s\n", err)
	}
}

// code for initializing the database from zero
var createTables = map[string]string{
	"config":    "config (key text primary key, value text)",
	"watchlist": "watchlist (name text primary key, chapter text)",
	"sites":     "sites (name text primary key, url text, priority integer, searchType integer, updated integer default 0)",
	"series":    "series (name text, key text, url text primary key, site text, updated integer default 0)",
	"chapters":  "chapters (name text, number text, url text primary key, series text, site text)",
	"log":       "log (message text, timestamp integer default 0)",
}

func (c *Config) initDb() {
	// start a transaction;  sqlite is slow as hell without them
	tx := c.db.MustBegin()
	defer tx.Commit()

	// create tables
	for _, c := range createTables {
		Execf(tx, "CREATE TABLE IF NOT EXISTS "+c)
	}

	isEmpty := func(table string) bool {
		var count int
		err := tx.Get(&count, "SELECT count(*) FROM "+table)
		if err != nil {
			log.Fatal(err)
		}
		return count == 0
	}

	if isEmpty("config") {
		Execf(tx, "insert into config (key, value) values (?, ?)", "DownloadPath", DefaultDownloadPath)
		Execf(tx, "insert into config (key, value) values (?, ?)", "UseUnderscores", "0")
	}

	if isEmpty("sites") {
		addSite := "insert into sites (name, url, priority, searchType) values (?, ?, ?, ?)"
		Execf(tx, addSite, "batoto", "http://bato.to", 1, searchTypeHTTP)
		Execf(tx, addSite, "mangapark", "http://mangapark.me", 2, searchTypeHTTP)
		Execf(tx, addSite, "mangahere", "http://mangahere.co", 3, searchTypeDirectory)
		Execf(tx, addSite, "mangatraders", "http://mangatraders.biz", 4, searchTypeHTTP)
		Execf(tx, addSite, "readcomiconline", "http://readcomiconline.to", 5, searchTypeHTTP)
		Execf(tx, addSite, "comicextra", "http://www.comicextra.com", 6, searchTypeHTTP)
	}

}

func (c *Config) SetDownloadPath(path string) {
	c.set("DownloadPath", path)
}

func (c *Config) AddSite(name, url string, priority int) error {
	// default to searchTypeHTTP because it's more common
	// eventually remove searchTypeDirectory
	_, err := c.db.Exec("insert into sites (name, url, priority, searchType) values (?, ?, ?, ?)",
		name, url, priority, searchTypeHTTP)
	return err
}

func (c *Config) RemoveSite(name string) error {
	if len(name) == 0 {
		return fmt.Errorf("Error: name of site to delete must be provided.")
	}
	_, err := c.db.Exec("DELETE FROM sites WHERE name=?", name)
	return err
}

func (c *Config) SetSitePriority(name string, priority int) {
	Execf(c.db, "update sites set priority=? where name=?", priority, name)
}

func (c *Config) Log(message string) error {
	now := time.Now().Unix()
	_, err := c.db.Exec("insert into log (timestamp, message) values (?, ?)",
		now, message)
	return err
}

func (c *Config) GetLog(numlines int) []Log {
	messages := make([]Log, 0, numlines)
	query := `SELECT timestamp, message FROM log ORDER BY timestamp DESC LIMIT ?`
	err := c.db.Select(&messages, query, numlines)
	if err != nil {
		fmt.Printf("Error occured fetching log: %s\n", err)
	}
	return messages
}
