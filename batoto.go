package main

import (
	"log"
	"net/url"
	"regexp"
	"strconv"
	"strings"

	"github.com/jmoiron/goquery"
)

const batotoSearchURL = "http://www.batoto.net/search_ajax?name="

type Batoto struct {
	Site
}

var _ Updater = &Batoto{}

func (b Batoto) SeriesListPath() string { return "" }
func (b Batoto) Name() string           { return b.Site.Name }

func (b Batoto) Search(terms ...string) []*Series {
	term := url.QueryEscape(strings.Join(terms, " "))
	searchURL := batotoSearchURL + term
	doc, err := GetDocument(b, searchURL)
	if err != nil {
		log.Println(err)
		return []*Series{}
	}
	return b.GetSeriesList(doc)
}

func (b Batoto) GetSeriesList(doc *goquery.Document) []*Series {
	anchors := doc.Find("table tr strong a").All()
	series := make([]*Series, 0, len(anchors))
	for _, a := range anchors {
		s := &Series{
			Site: "batoto",
			Name: trim(a.Text()),
			URL:  Attr(a, "href"),
		}
		spl := strings.Split(strings.TrimRight(s.URL, "/"), "/")
		s.Key = spl[len(spl)-1]
		series = append(series, s)
	}
	return series
}

// The trick here is to only choose english language chapters.
func (b Batoto) GetChapters(series *Series) []*Chapter {
	doc, err := GetDocument(b, series.URL)
	if err != nil {
		log.Println(err)
		return []*Chapter{}
	}
	return b.getChaptersFromDoc(doc, series.Name)
}

// getChaptersFromDoc parses the provided doc for chapters.
func (b Batoto) getChaptersFromDoc(doc *goquery.Document, seriesName string) []*Chapter {
	rows := doc.Find("table.chapters_list tr.lang_English").All()
	chapters := make([]*Chapter, 0, len(rows))
	numRe := regexp.MustCompile(`ch` + numRegexStr)
	base := BaseURL(doc.Url.String())

	for _, row := range rows {
		a := row.Find("a").First()
		c := &Chapter{
			Site:   "batoto",
			Series: seriesName,
			Name:   trim(a.Text()),
			URL:    BaseJoin(base, trim(Attr(a, "href"))),
		}
		if strings.Contains(c.Name, ":") {
			c.Name = trim(strings.SplitN(c.Name, ":", 2)[1])
		}
		c.Number = strings.TrimLeft(FindNumber(numRe.FindString(c.URL)), " \t0")
		c.Numberf, _ = strconv.ParseFloat(c.Number, 64)
		chapters = append(chapters, c)
	}
	reversed := make([]*Chapter, 0, len(chapters))
	for i := len(chapters) - 1; i >= 0; i-- {
		reversed = append(reversed, chapters[i])
	}
	return reversed
}

func (b Batoto) GetPageURLs(doc *goquery.Document) []string {
	options := doc.Find("#page_select").First().Find("option").All()
	if len(options) <= 1 {
		return []string{}
	}
	options = options[1:]

	urls := make([]string, 0, len(options))
	base := BaseURL(doc.Url.String())

	for _, o := range options {
		fragment := Attr(o, "value")
		urls = append(urls, BaseJoin(base, fragment))
	}
	return urls
}

func (b Batoto) GetImageURL(doc *goquery.Document) string {
	return Attr(doc.Find("#comic_page"), "src")
}
