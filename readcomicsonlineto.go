package main

import (
	"errors"
	"log"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"strconv"
	"strings"

	scraper "github.com/cardigann/go-cloudflare-scraper"
	"github.com/jmoiron/goquery"
)

// site loader for readcomiconline.to

const rcoSearchURL = "http://readcomiconline.to/Search/SearchSuggest"

type ReadComicOnline struct {
	Site
}

func (r ReadComicOnline) SeriesListPath() string { return "" }
func (r ReadComicOnline) Name() string           { return r.Site.Name }

func saveSession() {
	f, err := os.OpenFile(cfSessionPath, os.O_CREATE|os.O_RDWR, 0644)
	if err != nil {
		vprintf("could not create cfsession at %s: %s", cfSessionPath, err)
		return
	}
	cfClient.Save(f)
	f.Close()
}

func (r ReadComicOnline) Get(url string) (*goquery.Document, error) {
	resp, err := cfClient.Get(url)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode >= 400 {
		cfClient.Reset()
		resp, err = cfClient.Get(url)
		if resp.StatusCode >= 400 {
			return nil, errors.New(resp.Status)
		}
	}
	defer saveSession()
	return DocumentFromResponse(resp, url)
}

func (r ReadComicOnline) Init() {}

func (b ReadComicOnline) Search(terms ...string) []*Series {
	params := url.Values{
		"type":    []string{"Comic"},
		"keyword": []string{url.QueryEscape(strings.Join(terms, " "))},
	}

	vprintf("searching %s=%v", "comic", params["keyword"])
	tp, err := scraper.NewTransport(http.DefaultTransport)
	if err != nil {
		log.Printf("error creating scraper: %s", err)
		return nil
	}
	c := &http.Client{Transport: tp}

	resp, err := PostForm(rcoSearchURL, params, c)
	for resp.StatusCode == 503 {
		vprintf("trying again (503)")
		resp, err = PostForm(rcoSearchURL, params, c)
	}

	switch {
	case err != nil:
		log.Printf("Error posting search form: %s", err)
		return nil
	case resp.StatusCode >= 400:
		log.Printf("Error fetching %s: %s", rcoSearchURL, resp.Status)
		return nil
	}

	doc, err := DocumentFromResponse(resp, rcoSearchURL)
	if err != nil {
		log.Printf("Fetching search document: %s", err)
		return nil
	}

	return b.GetSeriesList(doc)
}

// GetSeriesList here reads the ajax search API
func (b ReadComicOnline) GetSeriesList(doc *goquery.Document) []*Series {
	anchors := doc.Find("a").All()
	vprintf("get series list: %d", len(anchors))
	series := make([]*Series, 0, len(anchors))
	for _, a := range anchors {
		s := &Series{
			Site: "readcomiconline",
			Name: trim(a.Text()),
			URL:  Attr(a, "href"),
		}
		spl := strings.Split(strings.TrimRight(s.URL, "/"), "/")
		s.Key = spl[len(spl)-1]
		series = append(series, s)
	}
	return series
}

// The trick here is to only choose english language chapters.
func (b ReadComicOnline) GetChapters(series *Series) []*Chapter {
	doc, err := b.Get(series.URL)
	if err != nil {
		log.Println(err)
		return []*Chapter{}
	}

	return b.getChaptersFromDoc(doc, series.Name)
}

// getChaptersFromDoc parses the provided doc for chapters.
func (b ReadComicOnline) getChaptersFromDoc(doc *goquery.Document, seriesName string) []*Chapter {
	rows := doc.Find("table.listing tbody tr td a").All()
	chapters := make([]*Chapter, 0, len(rows))
	// we replace - with . when we do the regex find for the number
	numRe := regexp.MustCompile(`Issue.` + numRegexStr)
	base := BaseURL(doc.Url.String())

	for _, a := range rows {
		c := &Chapter{
			Site:   "readcomiconline",
			Series: seriesName,
			Name:   trim(a.Text()),
			URL:    BaseJoin(base, trim(Attr(a, "href"))) + "&readType=1",
		}
		if strings.Contains(c.Name, "Issue #") {
			c.Name = trim(strings.SplitN(c.Name, "Issue #", 2)[0])
		}
		// Issue-12-1 for 12.1
		dashdot := strings.Replace(c.URL, "-", ".", -1)
		c.Number = strings.TrimLeft(FindNumber(numRe.FindString(dashdot)), " \t0")
		c.Numberf, _ = strconv.ParseFloat(c.Number, 64)
		chapters = append(chapters, c)
	}
	reversed := make([]*Chapter, 0, len(chapters))
	for i := len(chapters) - 1; i >= 0; i-- {
		reversed = append(reversed, chapters[i])
	}
	return reversed
}

func (b ReadComicOnline) GetPageURLs(doc *goquery.Document) []string {
	return []string{}
}

func (b ReadComicOnline) GetImageURL(doc *goquery.Document) string {
	return Attr(doc.Find("#divImgage img"), "src")
}

func (b ReadComicOnline) GetImageURLs(doc *goquery.Document) []string {
	var srcs []string
	for _, script := range doc.Find("script").All() {
		content := script.Text()
		if !strings.Contains(content, "lstImages.push(") {
			continue
		}
		urlRe := regexp.MustCompile(`lstImages.push\("(.*)"\);`)
		matches := urlRe.FindAllStringSubmatch(content, -1)
		for _, m := range matches {
			srcs = append(srcs, m[1])
		}
	}
	return srcs
}
