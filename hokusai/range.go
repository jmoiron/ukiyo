package main

// routines for turning text ranges into numbers-as-strings to insert
// into a URL template.

import (
	"fmt"
	"strconv"
	"strings"
)

// parseSpan turns any kind of span of ints into a list of strings.
// integer spans are presented with the original formatting, so that
// eg. "001-090" turns into %03d formatting.
//
// Negative numbers are not supported.
func parseSpan(span string) []string {
	var (
		final []string
		parts = strings.Split(span, ",")
	)
	for _, part := range parts {
		part = strings.TrimSpace(part)
		isRange := strings.IndexRune(part, '-') > 0
		if !isRange {
			final = append(final, part)
			continue
		}
		final = append(final, parseRange(part)...)
	}
	return final
}

// parseRange expands a range of strings using the width of the first
// number to provide leading zeroes for the rest, so eg `1-3` becomes
// [1,2,3], but `001-003` becomes [001, 002, 003].
func parseRange(r string) []string {
	spl := strings.Split(r, "-")
	if len(spl) != 2 {
		panic(fmt.Sprintf("invalid range %s", r))
	}
	start, end := spl[0], spl[1]
	istart, err := strconv.Atoi(start)
	if err != nil {
		panic(fmt.Sprintf("invalid int %s", start))
	}
	iend, err := strconv.Atoi(end)
	if err != nil {
		panic(fmt.Sprintf("invalid int %s", end))
	}

	var rng []string
	fmtter := fmt.Sprintf("%%0%dd", len(start))
	for i := istart; i <= iend; i++ {
		rng = append(rng, fmt.Sprintf(fmtter, i))
	}
	return rng
}
