package main

import (
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"

	scraper "github.com/jmoiron/go-cloudflare-scraper"
	"github.com/jmoiron/goquery"
)

type document struct {
	url string
	doc *goquery.Document
}

func fromResponse(r *http.Response, uri string) (*document, error) {
	d, err := goquery.NewDocumentFromResponse(r)
	if err != nil {
		return nil, err
	}
	d.Url, _ = url.Parse(uri)
	return &document{url: uri, doc: d}, nil
}

func defaultClient() *http.Client {
	scraper, err := scraper.NewTransport(http.DefaultTransport)
	if err != nil {
		panic(err)
	}
	return &http.Client{Transport: scraper}
}

// HttpDownloadTo downloads a url directly to a file.
func HttpDownloadTo(url, path string, client *http.Client) error {
	resp, err := client.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// sniff filetype from header instead
	if strings.HasSuffix(path, ".") {
		ct := resp.Header.Get("Content-Type")
		ctp := strings.Split(ct, "/")
		if len(ctp) > 1 && ctp[0] == "image" {
			path = path + ctp[1]
		}
	}

	file, err := os.Create(path)
	if err != nil {
		return err
	}
	defer file.Close()
	_, err = io.Copy(file, resp.Body)
	return err
}

// URLJoin joins multiple url bits into one
func URLJoin(strs ...string) string {
	ss := make([]string, len(strs))
	for i, s := range strs {
		if i == 0 {
			ss[i] = strings.TrimRight(s, "/")
		} else {
			ss[i] = strings.TrimLeft(s, "/")
		}
	}
	return strings.Join(ss, "/")
}

// BaseJoin joins the query to base if the query does not start with "http",
// otherwise returns the query as is.
func BaseJoin(base, query string) string {
	if strings.HasPrefix(query, "http") {
		return query
	}
	return URLJoin(base, query)
}

func Attr(s *goquery.Selection, attr string) string {
	val, ok := s.Attr(attr)
	if !ok {
		log.Printf("Could not find expected attr %s on %v\n", attr, s)
	}
	return val
}
