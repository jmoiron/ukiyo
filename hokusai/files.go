// small library to handle files and paths

package main

import (
	"archive/zip"
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/user"
	"path/filepath"
	"strings"
)

// return whether a path exists
func exists(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	log.Fatal(err)
	return false
}

// return a path with posix tilde and ENV expansion
func expandpath(path string) string {
	if path[0] == '~' {
		sep := strings.Index(path, string(os.PathSeparator))
		if sep < 0 {
			sep = len(path)
		}
		var err error
		var u *user.User
		username := path[1:sep]
		if len(username) == 0 {
			u, err = user.Current()
		} else {
			u, err = user.Lookup(username)
		}
		if err == nil {
			path = filepath.Join(u.HomeDir, path[sep:])
		}
	}
	path = os.ExpandEnv(path)
	abs, err := filepath.Abs(path)
	if err != nil {
		return path
	}
	return abs
}

// zip a directory (path) into destination path (dest).  because requirements
// for ukiyo are simple, it is non-recursive
func ZipDir(path, dest string) error {
	files, err := ioutil.ReadDir(path)
	if err != nil {
		return err
	}
	if len(files) == 0 {
		return fmt.Errorf("Nothing to zip.")
	}

	// create the destination zip file
	d, err := os.Create(dest)
	if err != nil {
		return err
	}
	err = d.Chmod(0644)
	if err != nil {
		return err
	}
	// create a zip writer on that file
	zipfile := zip.NewWriter(bufio.NewWriter(d))

	for _, file := range files {
		// don't bother with directories
		if file.IsDir() {
			continue
		}

		realpath := filepath.Join(path, file.Name())
		zippath := filepath.Join(filepath.Base(path), file.Name())
		f, err := zipfile.Create(zippath)
		if err != nil {
			return err
		}
		srcf, err := os.Open(realpath)
		if err != nil {
			return err
		}
		src := bufio.NewReader(srcf)
		_, err = io.Copy(f, src)
		if err != nil {
			return err
		}
	}
	err = zipfile.Close()
	if err != nil {
		return err
	}
	return nil
}
