package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"regexp"
	"runtime"
	"strings"

	optarg "github.com/jmoiron/go-pkg-optarg"
)

const (
	version = "0.1"
	usage   = `
Fetches comic chapters from one or more URLs, automatically creating
zip files in cwd.  URLs can contain a '{}' template, and you can provide
ranges via simple "1,2,5-10" syntax.  These numbers are used as chapter
numbers.  Eg:

hokusai -n "Hajime no Ippo" -u "http://mangasite.to/hajime_no_ippo/c{}/" -c "001-040"
`
)

var (
	DataHome   string
	ConfigHome string
	opts       = struct {
		verbose  bool
		preview  bool
		name     string
		chapters []string
		url      string
		selector string
		regex    *regexp.Regexp
	}{}
)

// getenv returns the environ value for key, or fallback if not found.
func getenv(key, fallback string) string {
	if value := os.Getenv(key); len(value) > 0 {
		return value
	}
	return fallback
}

func init() {
	switch runtime.GOOS {
	case "linux":
		DataHome = getenv("XDG_DATA_HOME", expandpath("~/.local/share"))
		ConfigHome = getenv("XDG_CONFIG_HOME", expandpath("~/.config"))
	case "darwin", "osx":
		DataHome = expandpath("~/Library/Application Support/")
		ConfigHome = expandpath("~/Library/Preferences/")
	}
}

func main() {
	var err error

	optarg.Add("h", "help", "Show help.", false)
	optarg.Add("", "version", "Show version and exit.", false)
	optarg.Add("v", "verbose", "Show more output.", false)

	optarg.Header("Downloading")
	optarg.Add("n", "name", "Series name.", "")
	optarg.Add("c", "chapters", "Chapters for templating.  Eg 1; 01-04, etc.", "")
	optarg.Add("u", "url", "Base url.  Use {} for chapter number template.", "")
	optarg.Add("i", "selector", "Selector for images.", "")
	optarg.Add("", "regex", "Regex for image URLs.", "")
	optarg.Add("", "preview", "Show urls but do not download.", false)

	for opt := range optarg.Parse() {
		switch opt.Name {
		case "help":
			optarg.Usage()
			fmt.Println(usage)
			return
		case "version":
			fmt.Printf("v%s\n", version)
			return
		case "verbose":
			opts.verbose = opt.Bool()
		case "name":
			opts.name = opt.String()
		case "chapters":
			opts.chapters = parseSpan(opt.String())
		case "url":
			opts.url = opt.String()
		case "preview":
			opts.preview = opt.Bool()
		case "selector":
			opts.selector = opt.String()
		case "regex":
			opts.regex, err = regexp.Compile(opt.String())
			if err != nil {
				fmt.Printf("Error: %s\n", err)
				os.Exit(-1)
			}
		}
	}

	if err := checkArgs(); err != nil {
		fmt.Printf("Error: %s", err)
		os.Exit(-1)
	}

	client := defaultClient()

	for _, chapter := range opts.chapters {
		uri := template(opts.url, chapter)
		fmt.Println(uri)

		if !opts.preview {
			r, err := client.Get(uri)
			if err != nil {
				fmt.Printf("Error: %s\n", err)
				os.Exit(-1)
			}
			fmt.Printf("%+v\n", r)
			body, err := ioutil.ReadAll(r.Body)
			if err != nil {
				fmt.Printf("Error: %s\n", err)
				os.Exit(-1)
			}
			fmt.Println(string(body))
		}
	}

}

func checkArgs() error {
	if strings.Index(opts.url, "{}") < 0 && len(opts.chapters) > 1 {
		return errors.New("no URL templating but more than one chapter requested")
	}
	// download something when chapters is not provided
	if len(opts.chapters) == 0 {
		opts.chapters = []string{"{}"}
	}
	return nil
}

func template(uri, num string) string {
	return strings.Replace(uri, "{}", num, -1)
}
