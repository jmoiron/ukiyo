package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSpan(t *testing.T) {
	assert := assert.New(t)
	assert.Equal([]string{"1"}, parseSpan("1"))
	assert.Equal([]string{"1", "2", "3"}, parseSpan("1,2,3"))
	assert.Equal([]string{"01", "02", "03"},
		parseSpan("01, 02, 03"))
}

func TestRange(t *testing.T) {
	assert := assert.New(t)

	assert.Equal([]string{"1", "2", "3"}, parseRange("1-3"))
	assert.Equal([]string{"01", "02", "03"}, parseRange("01-03"))

	val := parseRange("001-039")
	assert.Len(val, 39)
	for _, v := range val {
		assert.Len(v, 3)
	}
}
