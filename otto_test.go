package main

import (
	"bytes"
	"os"
	"testing"

	"github.com/jmoiron/goquery"
	"github.com/robertkrimen/otto"
	"github.com/stretchr/testify/assert"
)

func TestExtract(t *testing.T) {
	assert := assert.New(t)

	vm := otto.New()

	path := getenv("FILE", "./tests/cloudflare.html")
	f, err := os.Open(path)
	assert.NoError(err)
	assert.NotNil(f)

	doc, err := goquery.NewDocumentFromReader(f)
	assert.NoError(err)

	js, err := extractJS(doc)
	assert.NoError(err)
	assert.NotEmpty(js)

	val, err := vm.Eval(js)
	assert.NoError(err)
	assert.True(val.IsNumber())

	_, err = val.ToInteger()
	assert.NoError(err)
}

func TestCloudFlare(t *testing.T) {
	// remove to do a live test of cloudflare scraping
	t.Skip()

	assert := assert.New(t)
	// set up cookie jar and client
	jar := NewEncodedJar()
	c := NewCloudflareClientJar(jar)

	// fetch the IUAM page w/ js challenge
	resp, err := c.Get("http://readcomiconline.to")
	assert.NoError(err)

	doc, err := goquery.NewDocumentFromResponse(resp)
	assert.NoError(err)
	assert.Len(doc.Find("#containerRoot").All(), 1)

	var buf bytes.Buffer
	c.Save(&buf)
	assert.True(buf.Len() > 32)

	// load a default client..
	c2 := NewCloudflareClient()
	err = c2.Load(&buf)
	assert.NoError(err)

	resp, err = c2.Get("http://readcomiconline.to/Comic/Spider-Gwen-II")
	assert.NoError(err)

	doc, err = goquery.NewDocumentFromResponse(resp)
	assert.NoError(err)
	assert.Len(doc.Find("#containerRoot").All(), 1)

}
