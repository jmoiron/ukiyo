package main

// Routines to detect and play nicely with cloudflares under-attack mode
// which is always active on some sites.  Their layer-7 protection as of
// late 2016 is well-outlined here:
//
//  https://www.cryptobells.com/bypassing-cloudflares-layer-7-ddos-protection/
//
// The approach here is based off of the routines in the python cfscrape
// package:
//
// https://github.com/Anorov/cloudflare-scrape

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/jmoiron/goquery"
	"github.com/robertkrimen/otto"
)

// realisticUserAgents, required to pass cf's sniff tests
var realisticUserAgents = []string{
	"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36",
	"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36",
	"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0",
	"Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:41.0) Gecko/20100101 Firefox/41.0",
}

// randomAgent returns a random user agent, used to bootstrap clients
func randomAgent() string {
	return realisticUserAgents[rand.Intn(len(realisticUserAgents))]
}

// keep user-agent even on redirect:
// https://groups.google.com/forum/?fromgroups#!topic/golang-nuts/OwGvopYXpwE%5B1-25%5D
func keepUserAgent(req *http.Request, via []*http.Request) error {
	req.Header.Set("User-Agent", via[0].UserAgent())
	return nil
}

// A CloudflareClient is an http.Client capable of answering a cloudflare
// IUAM challenge.  The post-answered session is serializable, and can be
// stored and loaded for future requests.
type CloudflareClient struct {
	*http.Client
	ua         string
	challenged time.Time
}

// NewCloudflareClient returns a new CloudflareClient with a default cookiejar.
func NewCloudflareClient() *CloudflareClient {
	return NewCloudflareClientJar(NewEncodedJar())
}

// NewCloudflareClientJar returns a new CloudflareClient with the cookiejar
// provided.  You can use this to save and reuse cookies retrieved in
// previous sessions.
func NewCloudflareClientJar(jar http.CookieJar) *CloudflareClient {
	c := &http.Client{
		CheckRedirect: keepUserAgent,
		Jar:           jar,
	}
	return &CloudflareClient{Client: c, ua: randomAgent()}
}

// Reset the state of this client.  Still use the same user-agent, but clear
// the cookie jar and challenged status.
func (c *CloudflareClient) Reset() {
	c.Client.Jar = NewEncodedJar()
	c.challenged = time.Unix(0, 0)
}

// Get submits a Get request.  If this client has not been challenged in
// the last day, it sniffs the response to see if a challenge is present.
// If it is, then it executes the challenge and returns the resulting
// response.
func (c *CloudflareClient) Get(url string) (*http.Response, error) {
	resp, err := c.Client.Do(c.req("GET", url, nil))
	if err == nil && resp.StatusCode < 400 {
		return resp, err
	}
	return c.doChallenge(resp, url)
}

func PostForm(url string, data url.Values, client *http.Client) (*http.Response, error) {
	req, err := http.NewRequest("POST", url, strings.NewReader(data.Encode()))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Referer", BaseURL(url))
	req.Header.Set("Pragma", "no-cache")
	req.Header.Set("X-Requested-With", "XMLHttpRequest")
	return client.Do(req)
}

func (c *CloudflareClient) PostForm(url string, data url.Values) (*http.Response, error) {
	req := c.req("POST", url, strings.NewReader(data.Encode()))
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Referer", BaseURL(url))
	req.Header.Set("Pragma", "no-cache")
	req.Header.Set("X-Requested-With", "XMLHttpRequest")
	resp, err := c.Client.Do(req)
	if err == nil && resp.StatusCode < 400 {
		return resp, err
	}
	return c.doChallenge(resp, url)
}

// GetParams returns a GET request with the given url and GET params.
func (c *CloudflareClient) getParams(url string, params map[string]string) *http.Request {
	req := c.req("GET", url, nil)
	u := req.URL.Query()
	for key, value := range params {
		u.Add(key, value)
	}
	req.URL.RawQuery = u.Encode()
	return req
}

// req returns a basic request with this client's user agent.
func (c *CloudflareClient) req(method, url string, body io.Reader) *http.Request {
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("User-Agent", c.ua)
	return req
}

// Save an encoded session to the w.
func (c *CloudflareClient) Save(w io.Writer) error {
	ejar, ok := c.Jar.(*EncodedJar)
	if !ok {
		return errors.New("Cannot marshal session without use of encoded jar.")
	}
	var buf bytes.Buffer
	if err := ejar.Marshal(&buf); err != nil {
		return err
	}
	session := map[string]string{
		"jar": buf.String(),
		"ua":  c.ua,
		"ts":  fmt.Sprint(c.challenged.Unix()),
	}
	return json.NewEncoder(w).Encode(session)
}

// Load an encoded session saved by Save from r.
func (c *CloudflareClient) Load(r io.Reader) error {
	var session map[string]string
	err := json.NewDecoder(r).Decode(&session)
	if err != nil {
		return err
	}
	c.ua = session["ua"]
	if len(c.ua) == 0 {
		return errors.New("no user agent in session")
	}

	ts, err := strconv.ParseInt(session["ts"], 10, 64)
	if err != nil {
		return err
	}
	c.challenged = time.Unix(ts, 0)

	buf := bytes.NewBuffer([]byte(session["jar"]))
	c.Jar, err = LoadEncodedJar(buf)
	return err
}

// doChallenge attempts the cloudflare challenge on the response.  If it can't
// find a cloudflare challenge, it returns the original request with its
// original body.  If it can, it does the challenge and returns the response
// of the original URL.  If it encounters errors, it returns them along with
// either the original response or the cloudflare challenge response if it's
// already attempted it.
func (c *CloudflareClient) doChallenge(resp *http.Response, URL string) (*http.Response, error) {
	var rb bytes.Buffer
	io.Copy(&rb, resp.Body)
	// restore the response body in case we return it
	resp.Body = ioutil.NopCloser(&rb)

	// create a doc from the response
	document, err := goquery.NewDocumentFromReader(&rb)
	if err != nil {
		return resp, err
	}
	document.Url, _ = url.Parse(URL)

	// probably did not have a challenge form, so lets assume it's ok
	_, action, attrs, err := challengeForm(document)
	if err != nil {
		return resp, nil
	}

	// answer the challenge form
	var answer int
	answer, err = answerChallenge(document)
	vprintf("answering challenge form %v (%s)", answer, err)
	if err != nil {
		return resp, err
	}

	// set the answer, prepare the form submission
	attrs["jschl_answer"] = fmt.Sprint(answer)
	base := fmt.Sprintf("%s://%s", document.Url.Scheme, document.Url.Host)
	dest := URLJoin(base, action)

	req := c.getParams(dest, attrs)
	req.Header.Set("Referer", URL)

	// we found a challenge form and answered it, so we definitely have a challenge
	time.Sleep(5 * time.Second)
	c.challenged = time.Now()
	return c.Do(req)
}

// An EncodedJar can encode/decode cookies to a file for reuse across sessions.
type EncodedJar struct {
	*cookiejar.Jar
	urls []string
	mu   sync.Mutex
}

func (j *EncodedJar) SetCookies(u *url.URL, cookies []*http.Cookie) {
	j.mu.Lock()
	j.urls = append(j.urls, u.String())
	j.mu.Unlock()
	j.Jar.SetCookies(u, cookies)
}

// Marshal into an io.Writer, returning an error if encountered.
func (j *EncodedJar) Marshal(w io.Writer) error {
	j.mu.Lock()
	defer j.mu.Unlock()
	cookies := map[string][]*http.Cookie{}
	for _, u := range j.urls {
		parsed, err := url.Parse(u)
		if err != nil {
			return err
		}
		cookies[u] = append(cookies[u], j.Cookies(parsed)...)
	}
	return json.NewEncoder(w).Encode(cookies)
}

// NewEncodedJar returns a new EncodedJar with a clear cookiejar.
func NewEncodedJar() *EncodedJar {
	jar, err := cookiejar.New(nil)
	if err != nil {
		panic(err)
	}
	return &EncodedJar{Jar: jar}
}

// LoadEncodedJar loads a jar that has been saved by Marshal.
func LoadEncodedJar(r io.Reader) (*EncodedJar, error) {
	jar := NewEncodedJar()
	data := make(map[string][]*http.Cookie)
	err := json.NewDecoder(r).Decode(&data)
	if err != nil {
		return jar, err
	}

	for u, cookies := range data {
		l, err := url.Parse(u)
		if err != nil {
			log.Printf("Error: could not parse cookie url %s: %s\n", u, err)
			continue
		}

		jar.SetCookies(l, cookies)
	}
	return jar, nil
}

// extractJS extracts the javascript test from the document
func extractJS(doc *goquery.Document) (string, error) {
	scripts := doc.Find("script").All()

	if len(scripts) != 1 {
		return "", errors.New("expected only one script tag")
	}

	// use Text here because Html() escapes
	content := []byte(scripts[0].Text())

	pat := regexp.MustCompile(`setTimeout\(function\(\){\s+(var s,t,o,p,b,r,e,a,k,i,n,g,f.+?\r?\n[\s\S]+?a\.value =.+?)\r?\n`)
	subm := pat.FindSubmatch(content)
	if len(subm) != 2 {
		return "", errors.New("could not find cloudflare script; updated?")
	}
	content = subm[1]

	// regexp does not have submatch replacement
	pat = regexp.MustCompile(`a\.value = (parseInt\(.+?\)).+`)
	matches := pat.FindAllSubmatch(content, -1)
	for _, m := range matches {
		content = bytes.Replace(content, m[0], m[1], -1)
	}

	pat = regexp.MustCompile(`\s{3,}[a-z](?: = |\.).+`)
	content = pat.ReplaceAll(content, []byte{})

	return string(content), nil
}

// return the attributes from the challenge form, or an error if not found
func challengeForm(doc *goquery.Document) (method, action string, attrs map[string]string, err error) {

	form := doc.Find("#challenge-form")
	if form.Length() == 0 {
		err = errors.New("could not find #challenge-form")
		return
	}

	var ok bool
	if method, ok = form.Attr("method"); !ok {
		method = "get"
	}
	if action, ok = form.Attr("action"); !ok {
		// XXX: this.. may be dumb
		action = "/cdn-cgi/l/chk_jschl"
	}

	attrs = make(map[string]string)

	for _, input := range form.Find("input").All() {
		name := Attr(input, "name")
		value, _ := input.Attr("value")
		attrs[name] = value
	}

	return
}

func answerChallenge(doc *goquery.Document) (int, error) {
	vm := otto.New()
	js, err := extractJS(doc)
	if err != nil {
		return 0, err
	}

	js = "var a = {};" + js
	fmt.Println(js)
	val, err := vm.Eval(js)
	if err != nil {
		vprintf("error evaluating javascript")
		return 0, err
	}

	i, err := val.ToInteger()
	return int(i) + len(doc.Url.Host), err
}
