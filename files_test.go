package main

import (
	"archive/zip"
	"os"
	"os/user"
	"path"
	"path/filepath"
	"runtime"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestExpandPath(t *testing.T) {
	assert := assert.New(t)
	u, err := user.Current()
	assert.NoError(err)

	cwd := os.Getenv("PWD")
	home := u.HomeDir

	tests := [][2]string{
		{"foo", filepath.Join(cwd, "foo")},                      // ./foo
		{"~", home},                                             // /home/current/
		{"~foo", filepath.Join(cwd, "~foo")},                    // ./~foo
		{"~/foo", filepath.Join(home, "foo")},                   // /home/current/foo
		{"foo/~/", filepath.Join(cwd, "foo/~/")},                // ./foo/~/
		{"$HOME/foo", filepath.Join(home, "foo")},               // /home/current/foo
		{"~" + u.Username + "/foo", filepath.Join(home, "foo")}, // /home/current/foo
	}
	for _, t := range tests {
		assert.Equal(t[1], expandpath(t[0]))
	}
}

func TestExists(t *testing.T) {
	assert := assert.New(t)
	filename := "/tmp/_test_ukiyo.deleteok"
	f, err := os.Create(filename)
	assert.NoError(err)

	defer func() {
		f.Close()
		os.Remove(filename)
	}()

	assert.True(exists(filename), "cannot find "+filename)
	assert.False(exists("./fafawjaoeifjaowf.___.___"), "found bogus file in cwd")
}

func cwd() string {
	_, filename, _, _ := runtime.Caller(1)
	return path.Dir(filename)
}

func TestZipDir(t *testing.T) {
	testsDir := path.Join(cwd(), "tests")

	if !exists(testsDir) {
		t.Fatalf("Cannot find tests dir in \"%s\"  Run `./gen-tests.sh`", testsDir)
	}

	assert := assert.New(t)

	// TODO: we could test this more easily by zipping to any writer and
	// then providing a bytes buffer instead of using a temp file, but we'd
	// miss testing some of the file tests
	zipPath := "./__ukiyo-zipdir-test.zip"
	ZipDir(testsDir, zipPath)
	defer os.Remove(zipPath)

	// check permissions on file, should be 0644
	fi, err := os.Stat(zipPath)
	assert.NoError(err)
	assert.Equal(int(0644), int(fi.Mode()), "bad file mode")

	zf, err := zip.OpenReader(zipPath)
	assert.NoError(err)
	// it should have at least one file
	assert.NotEmpty(zf.File, "expected files in zip")

	// there should be one / in each file name
	for _, f := range zf.File {
		assert.Len(strings.Split(f.Name, "/"), 2, "expected one '/'")
	}
}
