package main

import (
	"log"
	"net/url"
	"strings"

	"github.com/jmoiron/goquery"
)

const (
	comicextraSearchURL = "http://www.comicextra.com/comic-search?key="
)

type Comicextra struct {
	Site
}

var _ Updater = &Comicextra{}

func (c Comicextra) SeriesListPath() string { return "" }
func (c Comicextra) Name() string           { return c.Site.Name }

func (c Comicextra) Search(terms ...string) []*Series {
	term := url.QueryEscape(strings.Join(terms, " "))
	searchURL := comicextraSearchURL + term
	doc, err := GetDocument(c, searchURL)
	if err != nil {
		log.Println(err)
		return []*Series{}
	}
	return c.GetSeriesList(doc)
}

func (c Comicextra) GetSeriesList(doc *goquery.Document) []*Series {
	elems := doc.Find("div.movie-list-index h3 a").All()
	series := make([]*Series, 0, len(elems))

	for _, a := range elems {
		name := a.Text()
		s := &Series{
			Site: c.Name(),
			Name: name,
			URL:  BaseJoin(c.URL, trim(a.AttrOr("href", ""))),
			Key:  name,
		}
		series = append(series, s)
	}

	vprintf("Found %d series for %s\n", len(series), c.Name())
	return series
}

func (c Comicextra) GetChapters(series *Series) []*Chapter {
	doc, err := GetDocument(c, series.URL)
	if err != nil {
		return []*Chapter{}
	}
	return c.getChaptersFromDoc(doc, series.Name)
}

// getChaptersFromDoc parses the provided doc for chapters.
func (c Comicextra) getChaptersFromDoc(doc *goquery.Document, seriesName string) []*Chapter {
	base := BaseURL(doc.Url.String())
	elems := doc.Find("#list tr td a").All()
	chapters := make([]*Chapter, 0, len(elems))

	for _, a := range elems {
		url := a.AttrOr("href", "")
		// assume if it's invalid to just keep going
		if len(url) == 0 || !strings.Contains(url, "/") {
			continue
		}
		spl := strings.Split(url, "/")
		num := FindNumber(spl[len(spl)-1])
		name := a.Text()

		if !strings.HasSuffix(url, "full") {
			url = BaseJoin(url, "/full")
		}

		ch := &Chapter{
			Site:   c.Name(),
			Series: seriesName,
			Name:   name,
			URL:    BaseJoin(base, url),
			Number: num,
		}
		chapters = append(chapters, ch)
	}
	vprintf("Found %d chapters on %s\n", len(chapters), c.Name())

	reversed := make([]*Chapter, 0, len(chapters))
	for i := len(chapters) - 1; i >= 0; i-- {
		reversed = append(reversed, chapters[i])
	}
	return reversed
}

func (c Comicextra) GetPageURLs(doc *goquery.Document) []string { return nil }
func (c Comicextra) GetImageURL(doc *goquery.Document) string   { return "" }

func (c Comicextra) GetImageURLs(doc *goquery.Document) []string {
	imgs := doc.Find("img.chapter_img").All()
	var urls []string
	for _, img := range imgs {
		url := img.AttrOr("src", "")
		if len(url) > 0 {
			urls = append(urls, url)
		}
	}
	return urls
}

// extractChapter attempts to get a chapter number.
func (c Comicextra) extractChapter(ch string) string {
	var candidate string

	spl := strings.Split(ch, "#")
	if len(spl) == 1 {
		return FindNumber(ch)
	}

	for _, s := range spl {
		p := FindNumber(s)
		// prefer ch.NN over anything else
		if strings.HasPrefix(s, "ch") && len(p) > 0 {
			return p
		}
		if len(candidate) > 0 {
			continue
		}
		candidate = p
	}
	return candidate
}
