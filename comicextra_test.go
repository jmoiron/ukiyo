package main

import (
	"net/url"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

var _ OnePageUpdater = &Comicextra{}

func TestComicextra(t *testing.T) {
	assert := assert.New(t)
	var ce Comicextra

	// test pulling series out of a search page
	doc := getDoc(t, "tests/comicextra-Search.html")
	ss := ce.GetSeriesList(doc)
	assert.NotEmpty(ss, "expected chapters but got none")
	for _, s := range ss {
		assert.NotEmpty(s.Name)
		assert.NotEmpty(s.URL)
	}

	// pulling chapters out of a volume page
	doc = getDoc(t, "tests/comicextra-Chapters.html")
	doc.Url, _ = url.Parse("http://www.comicextra.com/")
	cs := ce.getChaptersFromDoc(doc, "Series")
	assert.NotEmpty(cs)

	for _, c := range cs {
		assert.NotEmpty(c.URL)
		assert.NotEmpty(c.Number)
		assert.True(strings.HasSuffix(c.URL, "full"))
	}

	// pulling images out of a chapter page
	doc = getDoc(t, "tests/comicextra-Images.html")
	doc.Url, _ = url.Parse("http://www.comicextra.com/title/")
	urls := ce.GetImageURLs(doc)
	assert.NotEmpty(urls)
	assert.True(len(urls) > 5)
	for _, url := range urls {
		assert.True(strings.HasPrefix(url, "http"))
		assert.True(len(url) > 10)
	}
}
