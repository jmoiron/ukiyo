package main

import (
	"fmt"
)

const (
	white = iota + 89
	black
	red
	green
	yellow
	blue
	purple
)

// Color a string and return its colorized form
func Color(str string, color int, bold ...bool) string {
	Bold := DefaultFalse(bold...)
	BoldStr := ""
	if Bold {
		BoldStr = "01;"
	}
	return fmt.Sprintf("\033[%s%dm%s\033[0m", BoldStr, color, str)
}

func Bold(str string, color int) string {
	return Color(str, color, true)
}

var (
	White  = func(str string, bold ...bool) string { return Color(str, white, bold...) }
	Black  = func(str string, bold ...bool) string { return Color(str, black, bold...) }
	Red    = func(str string, bold ...bool) string { return Color(str, red, bold...) }
	Green  = func(str string, bold ...bool) string { return Color(str, green, bold...) }
	Yellow = func(str string, bold ...bool) string { return Color(str, yellow, bold...) }
	Blue   = func(str string, bold ...bool) string { return Color(str, blue, bold...) }
	Purple = func(str string, bold ...bool) string { return Color(str, purple, bold...) }
)
