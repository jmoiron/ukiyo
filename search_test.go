package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCompileRule(t *testing.T) {
	assert := assert.New(t)

	var rule *Rule

	rule = CompileRule("1")
	assert.True(rule.singular)
	assert.Equal(rule.lhs, "1")
	assert.Equal(rule.lhsf, 1.0)

	rule = CompileRule("!1")
	assert.True(rule.singular)
	assert.Equal(rule.lhs, "1")
	assert.Equal(rule.lhsf, 1.0)
	assert.True(rule.not)

	rule = CompileRule("<50")
	assert.True(rule.singular)
	assert.Equal(rule.lhs, "50")
	assert.Equal(rule.lhsf, 50.0)
	assert.True(rule.lt)

	rule = CompileRule("5-31")
	assert.True(rule.rng)
	assert.Equal(rule.lhs, "5")
	assert.Equal(rule.rhs, "31")
}

func TestApplyRule(t *testing.T) {
	assert := assert.New(t)
	var rule *Rule

	rule = CompileRule("<50")
	assert.True(rule.ApplyString("10"), "10 is < 50")
	assert.False(rule.ApplyString("50"), "50 is not < 50")

	rule = CompileRule(">=32")
	// XXX: ApplyString("100") fails because it does a string comparison, but it
	// appears as though this is actually desired..?
	assert.True(rule.ApplyString("32"), "32 is >= 32")
	assert.True(rule.ApplyFloat(100))

	rule = CompileRule("1-20")
	assert.True(rule.ApplyFloat(1))
	assert.True(rule.ApplyFloat(10))
	assert.True(rule.ApplyFloat(20))
	assert.False(rule.ApplyFloat(21))

	rule = CompileRule(">1.1")
	assert.True(rule.ApplyString("1.2"), "1.2 is > 1.1")

	// regression test
	rule = CompileRule(">240.6")
	assert.False(rule.ApplyString("240.1"))
}

func TestFilters(t *testing.T) {
	assert := assert.New(t)

	filter := "1,2,3,6-10"
	f := CompileFilter(filter)

	assert.True(f.Match("1"))
	assert.True(f.Match("3"))
	assert.True(f.Match("8"))
	assert.False(f.Match("5"))
}

func TestFindNUmber(t *testing.T) {
	assert := assert.New(t)

	res := FindNumber("The foo on the bus goes whee.")
	assert.Empty(res)

	res = FindNumber("12")
	assert.Equal("12", res)

	res = FindNumber("12.")
	assert.Equal("12", res)

	res = FindNumber("12.1")
	assert.Equal("12.1", res)

	res = FindNumber("123 12")
	assert.Equal("123", res)

	res = FindNumber("Toriko chapter 71\n")
	assert.Equal("71", res)
}
