package main

import (
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"os"
	"path/filepath"
	"strings"

	"github.com/jmoiron/goquery"
)

// This file implements an optional caching layer around the http client in
// the standard library as well as some convenience functions.  The cache is
// can be implemented as one of many backends, but a FileSystem based one
// is provided.

var ErrCacheMiss = errors.New("CacheMiss")

// keep user-agent even on redirect:
// https://groups.google.com/forum/?fromgroups#!topic/golang-nuts/OwGvopYXpwE%5B1-25%5D
func checkRedirect(req *http.Request, via []*http.Request) error {
	req.Header.Set("User-Agent", via[0].UserAgent())
	return nil
}

func client(jar http.CookieJar) *http.Client {
	var err error
	if jar == nil {
		jar, err = cookiejar.New(nil)
		if err != nil {
			panic(err)
		}
	}
	return &http.Client{
		CheckRedirect: checkRedirect,
		Jar:           jar,
	}
}

var (
	cfClient      = NewCloudflareClient()
	httpClient    = client(nil)
	cfSessionPath string
)

func init() {
	xdg.init()
	cfSessionPath = filepath.Join(xdg.ConfigHome, "ukiyo", "cfsession.json")

	f, err := os.Open(cfSessionPath)
	if err != nil {
		return
	}
	defer f.Close()
	err = cfClient.Load(f)
	if err != nil {
		vprintf("Cloudflare client could not find session: %s", err)
	}
}

func DocumentFromResponse(r *http.Response, u string) (*goquery.Document, error) {
	doc, err := goquery.NewDocumentFromResponse(r)
	if err != nil {
		return doc, err
	}
	doc.Url, _ = url.Parse(u)
	return doc, nil
}

// HttpDownloadTo downloads a url directly to a file.
func HttpDownloadTo(url, path string) error {
	resp, err := httpClient.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// sniff filetype from header instead
	if strings.HasSuffix(path, ".") {
		ct := resp.Header.Get("Content-Type")
		ctp := strings.Split(ct, "/")
		if len(ctp) > 1 && ctp[0] == "image" {
			path = path + ctp[1]
		}
	}

	file, err := os.Create(path)
	if err != nil {
		return err
	}
	defer file.Close()
	_, err = io.Copy(file, resp.Body)
	return err
}

// BaseURL returns return the base url for a given url string
func BaseURL(u string) string {
	parsed, _ := url.Parse(u)
	return fmt.Sprintf("%s://%s", parsed.Scheme, parsed.Host)
}

// URLJoin joins multiple url bits into one
func URLJoin(strs ...string) string {
	ss := make([]string, len(strs))
	for i, s := range strs {
		if i == 0 {
			ss[i] = strings.TrimRight(s, "/")
		} else {
			ss[i] = strings.TrimLeft(s, "/")
		}
	}
	return strings.Join(ss, "/")
}

// BaseJoin joins the query to base if the query does not start with "http",
// otherwise returns the query as is.
func BaseJoin(base string, query string) string {
	if strings.HasPrefix(query, "http") {
		return query
	}
	return URLJoin(base, query)
}

func Attr(s *goquery.Selection, attr string) string {
	val, ok := s.Attr(attr)
	if !ok {
		log.Printf("Could not find expected attr %s on %v\n", attr, s)
	}
	return val
}
