// This file contains parsing functionlity, both as a wrapper for
// underlying html parsing backends (for a more specialized, convenient
// interface) and for various parsing duties done by the rippers

package main

import (
	"regexp"
	"strings"
	"time"
)

var numRegexStr = `(\d+(?:\.\d+)?)`
var numRegex = regexp.MustCompile(numRegexStr)

// Find a number in a string
func FindNumber(str string) string {
	groups := numRegex.FindStringSubmatch(str)
	if groups == nil {
		return ""
	}
	return groups[1]
}

// given a unix timestamp, return a formatted date as a string
func toDate(timestamp int64) string {
	if timestamp == 0 {
		return "never"
	}
	return time.Unix(timestamp, 0).Format(time.UnixDate)
}

func FileExtension(str string) string {
	spl := strings.Split(str, ".")
	ext := spl[len(spl)-1]
	ext = strings.Split(ext, "?")[0]
	return ext
}

func trim(s string) string {
	return strings.Trim(s, " \t\r\n")
}
