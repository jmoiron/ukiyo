package main

import (
	"log"
	"net/url"
	"strings"

	"github.com/jmoiron/goquery"
)

const (
	mangaparkSearchURL = "http://mangapark.me/search?orderby=views&q="
)

type Mangapark struct {
	Site
}

// extractChapter attempts to get a chapter number.
func (m Mangapark) extractChapter(c string) string {
	var candidate string

	spl := strings.Split(c, " ")
	if len(spl) == 1 {
		return FindNumber(c)
	}

	for _, s := range spl {
		p := FindNumber(s)
		// prefer ch.NN over anything else
		if strings.HasPrefix(s, "ch") && len(p) > 0 {
			return p
		}
		if len(candidate) > 0 {
			continue
		}
		candidate = p
	}
	return candidate
}

var _ Updater = &Mangapark{}

func (m Mangapark) SeriesListPath() string { return "" }
func (m Mangapark) Name() string           { return m.Site.Name }

func (m Mangapark) Search(terms ...string) []*Series {
	term := url.QueryEscape(strings.Join(terms, " "))
	searchURL := mangaparkSearchURL + term
	doc, err := GetDocument(m, searchURL)
	if err != nil {
		log.Println(err)
		return []*Series{}
	}
	return m.GetSeriesList(doc)
}

func (m Mangapark) GetSeriesList(doc *goquery.Document) []*Series {
	elems := doc.Find("div.manga-list div.item h2 a").All()
	series := make([]*Series, 0, len(elems))

	for _, a := range elems {
		name := a.AttrOr("title", a.Text())
		s := &Series{
			Site: m.Name(),
			Name: name,
			URL:  BaseJoin(m.URL, trim(a.AttrOr("href", ""))),
			Key:  name,
		}
		series = append(series, s)
	}

	vprintf("Found %d series for %s\n", len(series), m.Name())
	return series
}

func (m Mangapark) GetChapters(series *Series) []*Chapter {
	doc, err := GetDocument(m, series.URL)
	if err != nil {
		return []*Chapter{}
	}
	return m.getChaptersFromDoc(doc, series.Name)
}

func getAllURL(anchors *goquery.Selection) string {
	for _, a := range anchors.All() {
		if a.Text() == "all" {
			return a.AttrOr("href", "")
		}
	}
	return ""
}

// getChaptersFromDoc parses the provided doc for chapters.
func (m Mangapark) getChaptersFromDoc(doc *goquery.Document, seriesName string) []*Chapter {
	base := BaseURL(doc.Url.String())
	elems := doc.Find("div.stream:not(.collapsed) ul.chapter li").All()
	chapters := make([]*Chapter, 0, len(elems))

	for _, li := range elems {
		num := m.extractChapter(li.Find("span a").Text())
		url := getAllURL(li.Find("a"))
		name := li.Find("span").Text()

		c := &Chapter{
			Site:   m.Name(),
			Series: seriesName,
			Name:   name,
			URL:    BaseJoin(base, url),
			Number: num,
		}
		chapters = append(chapters, c)
	}
	vprintf("Found %d chapters on mangahere\n", len(chapters))

	reversed := make([]*Chapter, 0, len(chapters))
	for i := len(chapters) - 1; i >= 0; i-- {
		reversed = append(reversed, chapters[i])
	}
	return reversed
}

func (m Mangapark) GetPageURLs(doc *goquery.Document) []string { return nil }
func (m Mangapark) GetImageURL(doc *goquery.Document) string   { return "" }

func (m Mangapark) GetImageURLs(doc *goquery.Document) []string {
	imgs := doc.Find("#viewer a.img-link img").All()
	var urls []string
	for _, img := range imgs {
		url := img.AttrOr("src", "")
		if len(url) > 0 {
			urls = append(urls, url)
		}
	}
	return urls
}
