package main

import (
	"net/url"
	"os"
	"testing"

	"github.com/jmoiron/goquery"
	"github.com/stretchr/testify/assert"
)

var updaters = map[string]Updater{
	"mangahere":    Mangahere{Site: Site{Name: "mangahere"}},
	"mangatraders": Mangatraders{Site: Site{Name: "mangatraders"}},
}

func getDoc(t *testing.T, path string) *goquery.Document {
	// XXX: this leaks open files
	file, err := os.Open(path)
	if err != nil {
		t.Fatal(err)
	}
	doc, err := goquery.NewDocumentFromReader(file)
	if err != nil {
		t.Fatal(err)
	}
	return doc
}

func TestMangahereLicenceDetection(t *testing.T) {
	assert := assert.New(t)

	unlicensed := []string{
		"tests/mangahere-TestDetectNotLicensed1",
		"tests/mangahere-TestDetectNotLicensed2",
	}

	licensed := []string{
		"tests/mangahere-TestDetectLicensed1",
		"tests/mangahere-TestDetectLicensed2",
	}

	mh := updaters["mangahere"].(Mangahere)

	for _, path := range licensed {
		doc := getDoc(t, path)
		assert.Equal(true, mh.isLicensed(doc))
	}

	for _, path := range unlicensed {
		doc := getDoc(t, path)
		assert.Equal(false, mh.isLicensed(doc))
	}

}

func TestGetImageURL(t *testing.T) {
	assert := assert.New(t)

	tests := []struct {
		u   Updater
		url string
	}{
		{updaters["mangahere"], "tests/mangahere-TestGetImageUrl"},
		{updaters["mangatraders"], "tests/mangatraders-TestGetImageUrl"},
	}
	for _, test := range tests {
		doc := getDoc(t, test.url)
		url := test.u.GetImageURL(doc)
		assert.NotEmpty(url, "expected url from %v (%s)", test.u, test.url)
	}
}

func TestGetPageURLs(t *testing.T) {
	tests := []struct {
		u         Updater
		url, base string
		pages     int
	}{
		{updaters["mangahere"], "tests/mangahere-TestGetPageUrls", "http://mangahere.co/", 28},
		//	{updaters["mangatraders"], "tests/mangatraders-TestGetPageUrls", "http://mangatraders.org/", 19},
		// mangatraders is a "onepage" type
	}

	assert := assert.New(t)

	for _, test := range tests {
		var err error
		doc := getDoc(t, test.url)
		doc.Url, err = url.Parse(test.base)
		assert.NoError(err)
		assert.NotNil(doc)

		pages := test.u.GetPageURLs(doc)

		// verify the number of pages matches what is expected
		if len(pages) != test.pages {
			t.Errorf("Expected %d pages for %s, but got %d", test.pages, test.u.Name(), len(pages))
		}

		// verify they are all valid urls
		for _, p := range pages {
			_, err := url.Parse(p)
			if err != nil {
				t.Error(err)
			}
		}
	}
}

func TestGetChapters(t *testing.T) {
	type chapGetter interface {
		Updater
		getChaptersFromDoc(*goquery.Document, string) []*Chapter
	}
	tests := []struct {
		getter     chapGetter
		path, base string
	}{
		{updaters["mangahere"].(chapGetter), "tests/mangahere-TestGetChapters", "http://mangahere.co/"},
	}

	for _, test := range tests {
		doc := getDoc(t, test.path)
		doc.Url, _ = url.Parse(test.base)

		chapters := test.getter.getChaptersFromDoc(doc, "manga")
		if len(chapters) < 50 {
			t.Errorf("Expected lots of chapters from %s but only got %d.", test.getter.Name(), len(chapters))
		}
	}
}

func TestGetSeries(t *testing.T) {
	tests := []struct {
		u          Updater
		path, base string
	}{
		{updaters["mangahere"], "tests/mangahere-SeriesList", "http://mangahere.co/"},
		// mangatraders has a paginated list these days, so it's not suitable
		// for offline search...  offline search may be a bad idea anyway, if
		// the site is not available then it's probably gone and it's just
		// as likely that search will break/change as the directory
		//{updaters["mangatraders"], "tests/mangatraders-SeriesList", "http://mangatraders.org"},
	}

	for _, test := range tests {
		doc := getDoc(t, test.path)
		doc.Url, _ = url.Parse(test.base)

		series := test.u.GetSeriesList(doc)
		if len(series) == 0 {
			t.Errorf("Expected lots of series but got none from %s", test.u.Name())
		}
	}
}
