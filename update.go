package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/jmoiron/goquery"
)

// update once per week unless forced
var SiteUpdateFrequency = int64(86400) * 7

// update once per day unless forced
var ChapterUpdateFrequency = int64(86400)
var MaxConcurrentWorkers = 3

type Updater interface {
	// SeriesListPath returns a url path for the manga series list.  If the site
	// lacks such a path, it should return the empty string.
	SeriesListPath() string
	// Name returns the name of this updater.
	Name() string
	// GetChapters returns a list of Chapters for the series being updated.
	GetChapters(series *Series) []*Chapter
	// GetSeriesList takes a Document and returns a list of Series from it.
	GetSeriesList(*goquery.Document) []*Series
	// GetPageURLs returns a list of the urls (as strings) on the page.
	GetPageURLs(*goquery.Document) []string
	// GetImageURL returns the url of the image on the Document, which is a page.
	GetImageURL(*goquery.Document) string
	// Search does a live search against the website.  For sites that use full
	// lists and local searches, this should be a noop.
	Search(terms ...string) []*Series
}

type OnePageUpdater interface {
	Updater
	GetImageURLs(*goquery.Document) []string
}

type Getter interface {
	Get(string) (*goquery.Document, error)
}

// UpdateSeries uses an updater to load the cache of series at that site, if the
// site has a list of series available.  This allows local searching in sql, which
// makes searching much faster and hits the upstream site less.  Returns an empty
// list when SeriesListPath() returns the empty string.
func UpdateSeries(u Updater) []*Series {
	seriesURL := u.SeriesListPath()
	// If this updater doesn't have a series list, make a note and continue.
	if len(seriesURL) == 0 {
		log.Printf("Skipping %s series list: search interface only.\n", u.Name())
		return []*Series{}
	}

	vprintf("Updating %s via %s\n", u.Name(), seriesURL)

	resp, err := httpClient.Get(seriesURL)

	if err != nil {
		log.Printf("Error fetching %s series list: %s\n", u.Name(), err)
		return []*Series{}
	}

	document, err := DocumentFromResponse(resp, seriesURL)
	if err != nil {
		log.Printf("Error parsing document from %s", seriesURL)
		return []*Series{}
	}

	return u.GetSeriesList(document)
}

func GetDocument(up Updater, u string) (*goquery.Document, error) {
	resp, err := httpClient.Get(u)
	if err != nil {
		log.Printf("Error getting %s from %s\n", u, up.Name())
		return nil, err
	}

	return DocumentFromResponse(resp, u)
}

var UpdaterRegistry = map[string]Updater{}

func initUpdater() {
	sites := []Site{}
	err := config.db.Select(&sites, "SELECT * FROM sites")
	if err != nil {
		log.Fatal("listing sites:", err)
	}
	siteMap := map[string]Site{}
	for _, site := range sites {
		siteMap[site.Name] = site
	}

	for key, val := range siteMap {
		switch key {
		case "comicextra":
			UpdaterRegistry[key] = &Comicextra{val}
		case "mangapark":
			UpdaterRegistry[key] = &Mangapark{val}
		case "mangahere":
			UpdaterRegistry[key] = &Mangahere{val}
		case "batoto":
			UpdaterRegistry[key] = &Batoto{val}
		case "mangatraders":
			UpdaterRegistry[key] = &Mangatraders{val}
		case "readcomiconline":
			rco := &ReadComicOnline{val}
			rco.Init()
			UpdaterRegistry[key] = rco
		}
	}

	// If the Site is set to something which is search-only, use active searching
	// is forced to true.
	if len(opts.Site) > 0 {
		if u, ok := UpdaterRegistry[opts.Site]; ok && len(u.SeriesListPath()) == 0 {
			opts.UseActiveSearches = true
		}
	}
}

func UpdateSites(force ...bool) {
	Force := DefaultFalse(force...)

	var sites []Site
	now := time.Now().Unix()
	after := now
	if !Force {
		after -= SiteUpdateFrequency
	}

	q := "select name, url, priority, updated from sites WHERE updated < ? ORDER BY priority"
	err := config.db.Select(&sites, q, after)
	if err != nil {
		panic(err)
	}

	if len(sites) == 0 {
		return
	}

	if !Force {
		vprintf("Updating %d sites last updated over 1 week ago:\n", len(sites))
	} else {
		vprintf("Force-updating %d sites:\n", len(sites))
	}

	sem := make(chan bool, MaxConcurrentWorkers)
	results := []*Series{}

	for _, s := range sites {
		sem <- true
		go func(site Site) {
			defer func() {
				if r := recover(); r != nil {
					log.Printf("Recovered in %#v: %s\n", site, r)
				}
				<-sem
			}()
			updater, ok := UpdaterRegistry[site.Name]
			if !ok {
				log.Printf("Unknown site-name or no updater for %s, skipping update.\n", site.Name)
				return
			}
			ret := UpdateSeries(updater)
			results = append(results, ret...)
		}(s)
	}
	for i := 0; i < cap(sem); i++ {
		sem <- true
	}

	vprintf("Received %d total results\n", len(results))

	if err := upsertSeries(results...); err != nil {
		fmt.Printf("Error updating series: %s\n", err)
		return
	}

	tx := config.db.MustBegin()
	for _, s := range sites {
		tx.Exec("update sites set updated = ? where name = ?", now, s.Name)
	}
	tx.Commit()
}

func upsertSeries(series ...*Series) error {
	tx := config.db.MustBegin()

	q := `insert or replace into series 
		(name, key, url, site, updated) values 
		(?, ?, ?, ?, coalesce((select updated from series where url = ?), 0))`

	for _, r := range series {
		if _, err := tx.Exec(q, r.Name, r.Key, r.URL, r.Site, r.URL); err != nil {
			return err
		}
	}
	return tx.Commit()
}

func GetChapters(series *Series) []*Chapter {
	if opts.UseActiveSearches {
		return UpdaterRegistry[opts.Site].GetChapters(series)
	}
	return UpdateChapters(series.Name)
}

// update chapters for a series.  searches the db for all sites that have
// the series, and updates any which are too old for the chapters threshold
func UpdateChapters(name string, force ...bool) []*Chapter {
	Force := DefaultFalse(force...)

	now := time.Now().Unix()
	after := now
	if !Force {
		after -= ChapterUpdateFrequency
	}

	series := []*Series{}
	err := config.db.Select(&series, "select * from series where name = ?  AND updated < ?", name, after)
	if err != nil {
		fmt.Printf("Error: %s\n", err)
		return []*Chapter{}
	}
	if len(series) > 0 {
		if !Force {
			vprintf("Updating %d sites for %s last updated over 1 week ago:\n", len(series), name)
		} else {
			vprintf("Force-updating %d sites for %s:\n", len(series), name)
		}
		for _, s := range series {
			vprintf("Updating %s (@%s, %s)\n", s.Name, s.Site, s.URL)
		}

		sem := make(chan bool, MaxConcurrentWorkers)
		results := []*Chapter{}

		for _, s := range series {
			sem <- true
			go func(s *Series) {
				defer func() {
					if r := recover(); r != nil {
						fmt.Printf("Recovered in %v: %v", s, r)
					}
					<-sem
				}()
				updater, ok := UpdaterRegistry[s.Site]
				if !ok {
					fmt.Printf("Unknown site-name %s, skipping update.\n", s.Site)
					return
				}
				chapters := updater.GetChapters(s)
				results = append(results, chapters...)
			}(s)
		}
		for i := 0; i < cap(sem); i++ {
			sem <- true
		}

		tx, _ := config.db.Beginx()
		vprintf("Received %d total results\n", len(results))

		for _, s := range series {
			Execf(tx, "delete from chapters where name=? and site=?", name, s.Site)
		}

		q := `insert or replace into chapters 
		(name, number, url, series, site) values 
		(?, ?, ?, ?, ?)`

		for _, c := range results {
			Execf(tx, q, c.Name, c.Number, c.URL, c.Series, c.Site)
		}
		for _, s := range series {
			Execf(tx, "update series set updated = ? where name = ? and site= ?", now, name, s.Site)
		}
		tx.Commit()
	}

	chapters, _ := FindChapters(true, name)
	return chapters
}

func SelectURL(chapter *Chapter) (string, string) {
	spl := strings.Split(chapter.Site, ",")
	m := make(map[string]int, len(spl))
	for i, s := range spl {
		m[s] = i
	}
	for _, s := range config.SiteOrder {
		_, ok := m[s]
		if ok {
			spl = strings.Split(chapter.URL, ",")
			return s, spl[m[s]]
		}
	}
	return "", ""
}

// zipfile returns the zipfile name for a chapter, with the given version number.
// If the version is 0, then it is not used.
func zipfile(chapter *Chapter, version int) string {
	number := chapter.Number
	// for some reason, sometimes chaps are numbered '3.1', '3.5', etc
	if chapter.Numberf != 0 {
		fnum := fmt.Sprintf("%05.1f", chapter.Numberf)
		if fnum[len(fnum)-1] != '0' {
			number = fnum
		} else {
			number = fmt.Sprintf("%03d", int(chapter.Numberf))
		}
	}
	var verstr string
	if version > 0 {
		verstr = fmt.Sprintf("-v%d", version)
	}
	return fmt.Sprintf("%s-c%s%s.zip", chapter.SeriesString(), number, verstr)
}

func DownloadChapter(chapter *Chapter) error {
	site, url := SelectURL(chapter)
	vprintf(" %s %s (%s, %s)\n", chapter.Series, chapter.Number, site, url)
	updater := UpdaterRegistry[site]

	// see if the single page updater is implemented;  if so we can skip
	// a bunch of page loads and get the images directly
	_, onepage := updater.(OnePageUpdater)

	var (
		doc *goquery.Document
		err error
	)

	// some pages might require special getters..
	if getter, ok := updater.(Getter); ok {
		doc, err = getter.Get(url)
	} else {
		doc, err = GetDocument(updater, url)
	}

	if err != nil {
		fmt.Printf("Error fetching `%s`: %s\n", url, err)
		return err
	}

	destpath := filepath.Join(config.DownloadPath, chapter.SeriesString(), chapter.Number)
	destzip := filepath.Join(config.DownloadPath, chapter.SeriesString(), zipfile(chapter, 0))

	// Bump the version number until the file doesn't exist.
	if !opts.Overwrite {
		_, err = os.Stat(destzip)
		for v := 1; err == nil; v++ {
			destzip = filepath.Join(config.DownloadPath, chapter.SeriesString(), zipfile(chapter, v))
			_, err = os.Stat(destzip)
		}
	}

	os.MkdirAll(destpath, 0755)

	type Img struct {
		num int
		url string
	}

	var numImages int
	var images chan Img
	sem := make(chan bool, MaxConcurrentWorkers)
	completed := make(chan int)

	if onepage {
		opu := updater.(OnePageUpdater)
		imgURLs := opu.GetImageURLs(doc)
		numImages = len(imgURLs)
		images = make(chan Img, numImages)
		for i, url := range imgURLs {
			images <- Img{i + 1, url}
		}
	} else {
		pageURLs := updater.GetPageURLs(doc)
		// we add one here because the current page has an image but is not
		// included in the pageUrls
		numImages = len(pageURLs) + 1
		vprintf("Page URLs: %v\n", pageURLs)

		images = make(chan Img, len(pageURLs)+1)
		// send the first image on the images channel
		images <- Img{1, updater.GetImageURL(doc)}

		for i, s := range pageURLs {
			sem <- true
			go func(num int, url string) {
				defer func() { <-sem }()
				doc, err := GetDocument(updater, url)
				if err != nil {
					fmt.Printf("Error fetching page %03d (%s)\n", num, url)
					return
				}
				images <- Img{num, updater.GetImageURL(doc)}
			}(i+2, s)
		}
	}

	update := fmt.Sprintf("Downloading %s %s (from %s): %%d of %%d", chapter.Series, chapter.Number, site)
	// print a little updater in place about pages we're loading
	go func(max int) {
		c := 0
		fmt.Printf(update+"\r", c, max)
		os.Stdout.Sync()
		for _ = range completed {
			c++
			fmt.Printf(update+"\r", c, max)
			os.Stdout.Sync()
		}
		fmt.Printf(update+"\n", c, max)
	}(numImages)

	numwidth := len(fmt.Sprintf("%d", numImages))
	numfmt := fmt.Sprintf("%%0%dd", numwidth)

	for i := 0; i < numImages; i++ {
		sem <- true
		go func() {
			defer func() { <-sem }()
			img := <-images
			if len(img.url) == 0 {
				fmt.Printf("Error fetching page %03d (null url)\n", img.num)
				return
			}
			ext := FileExtension(img.url)
			if len(ext) > 5 {
				ext = ""
			}
			// destpath ends with . if we don't know the extension
			name := fmt.Sprintf(numfmt+".%s", img.num, ext)
			destfile := filepath.Join(destpath, name)
			// fmt.Printf("#%d (%s) to %s\n", img.num, img.url, destfile)
			err := HttpDownloadTo(img.url, destfile)
			if err != nil {
				fmt.Println(err)
			}
			completed <- 1
		}()
	}
	// wait for everything to finish...
	for i := 0; i < cap(sem); i++ {
		sem <- true
	}

	ZipDir(destpath, destzip)
	os.RemoveAll(destpath)
	close(completed)

	msg := fmt.Sprintf("Downloaded %s %s from %s", chapter.Series, chapter.Number, site)
	config.Log(msg)

	return nil
}
