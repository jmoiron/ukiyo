// functions to implement series following and updating
package main

import (
	"fmt"
	"log"
	"strconv"
)

// A ListItem is a member of the watchlist corresponding to a series.
type ListItem struct {
	Name    string
	Number  string `db:"chapter"`
	Numberf float64
}

// Updates is a container for a list of chapters for a series that is
// available.
type Updates struct {
	Series    ListItem
	Available []*Chapter
}

// A Watchlist is a list of series that are tracked.
type Watchlist struct {
	Items  []ListItem
	loaded bool
}

// Load the watchlist.
func (w *Watchlist) Load() {
	if w.loaded {
		return
	}
	err := config.db.Select(&w.Items, "select name, chapter from watchlist order by name;")
	if err != nil {
		log.Println(err)
		return
	}
	for _, i := range w.Items {
		i.Numberf, _ = strconv.ParseFloat(i.Number, 64)
	}
	w.loaded = true
}

func (w *Watchlist) update(force bool) {
	w.Load()
	for _, item := range w.Items {
		UpdateChapters(item.Name, force)
	}
}

// Update the list of available chapters for each series in the watchlist.
func (w *Watchlist) Update() {
	w.update(false)
}

// ForceUpdate the list of available chapters for each series in the watchlist.
func (w *Watchlist) ForceUpdate() {
	w.update(true)
}

// Add a series to the watchlist by name.
func (w *Watchlist) Add(name string) {
	tx, _ := config.db.Beginx()
	Execf(tx, "insert into watchlist (name, chapter) values (?, ?);", name, 0)
	tx.Commit()
}

// Remove a series from the watchlist.
func (w *Watchlist) Remove(name string) {
	tx, _ := config.db.Beginx()
	Execf(tx, "delete from watchlist where name=?;", name)
	tx.Commit()
}

// Set the chapter numbe for an item in the watchlist.
func (w *Watchlist) Set(name string, number string) error {
	var items []ListItem
	err := config.db.Select(&items, "select name, chapter from watchlist where name LIKE ?", name)
	if err != nil {
		return err
	}
	if len(items) != 1 {
		return fmt.Errorf("Expected one match from name \"%s\", but found %d!", name, len(items))
	}

	tx, _ := config.db.Beginx()
	_, err = tx.Exec("update watchlist set chapter=? where name=?", number, items[0].Name)
	tx.Commit()
	return err
}

// FindNew compiles a list of updates available for the watchlist.
func (w *Watchlist) FindNew() []Updates {
	w.Load()
	w.Update()

	var updates []Updates

	for _, item := range w.Items {
		update := Updates{Series: item}
		filter := CompileFilter(fmt.Sprintf(">%s", item.Number))
		chapters := UpdateChapters(item.Name)

		for _, chapter := range chapters {
			if filter.Match(chapter.Number) {
				update.Available = append(update.Available, chapter)
			}
		}
		updates = append(updates, update)
	}
	return updates
}
